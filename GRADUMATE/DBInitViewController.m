//
//  DBInitViewController.m
//  GRADUMATE
//
//  Created by Malek Mansour on 2/5/15.
//  Copyright (c) 2015 SlickStone. All rights reserved.
//

#import "DBInitViewController.h"
//#import "QUESTIONAIRE.h"



@interface DBInitViewController ()

@end

@implementation DBInitViewController

-(void)Insert //launch0-2
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"GRADUMATE.sqlite"];
    FMDatabase* db = [FMDatabase databaseWithPath:writableDBPath];
    if(![db open])
    {
        NSLog(@"Err %d: %@",[db lastErrorCode],[db lastErrorMessage]);
    }
    
    [db setShouldCacheStatements:YES];
    
    //Insert Into Questions
    [db executeUpdate:@"INSERT INTO GRAD_QUESTIONS(NAME, ID_QUESTIONNAIRE) Values('Tell me about yourself',1)"];
    [db executeUpdate:@"INSERT INTO GRAD_QUESTIONS(NAME, ID_QUESTIONNAIRE) Values('Describe your perfect job',1)"];
    [db executeUpdate:@"INSERT INTO GRAD_QUESTIONS(NAME, ID_QUESTIONNAIRE) Values('Tell me something that is a challenge for you',1)"];
    [db executeUpdate:@"INSERT INTO GRAD_QUESTIONS(NAME, ID_QUESTIONNAIRE) Values('What are you proud of',1)"];
    [db executeUpdate:@"INSERT INTO GRAD_QUESTIONS(NAME, ID_QUESTIONNAIRE) Values('How do you resolve issues in a team?',1)"];
    
    [db executeUpdate:@"INSERT INTO GRAD_QUESTIONS(NAME, ID_QUESTIONNAIRE) Values('What were your favorite engineering classes? Why?',2)"];
    [db executeUpdate:@"INSERT INTO GRAD_QUESTIONS(NAME, ID_QUESTIONNAIRE) Values('How would you describe your educational experience',2)"];
    [db executeUpdate:@"INSERT INTO GRAD_QUESTIONS(NAME, ID_QUESTIONNAIRE) Values('Are you happy with your academic performance',2)"];
    [db executeUpdate:@"INSERT INTO GRAD_QUESTIONS(NAME, ID_QUESTIONNAIRE) Values('What would you change about your academic experience',2)"];
    [db executeUpdate:@"INSERT INTO GRAD_QUESTIONS(NAME, ID_QUESTIONNAIRE) Values('Do you have a license',2)"];
    
    
    [db executeUpdate:@"INSERT INTO GRAD_QUESTIONS(NAME, ID_QUESTIONNAIRE) Values('How would you describe your technical knowledge of Microsoft Access and Excel',3)"];
    [db executeUpdate:@"INSERT INTO GRAD_QUESTIONS(NAME, ID_QUESTIONNAIRE) Values('How would you present some basic data  to the main department',3)"];
    [db executeUpdate:@"INSERT INTO GRAD_QUESTIONS(NAME, ID_QUESTIONNAIRE) Values('What achievement are you most proud of',3)"];
    
    
    [db executeUpdate:@"INSERT INTO GRAD_QUESTIONS(NAME, ID_QUESTIONNAIRE) Values('At which point do you find it necessary to bring others into your decision-making process? Why?',4)"];
    [db executeUpdate:@"INSERT INTO GRAD_QUESTIONS(NAME, ID_QUESTIONNAIRE) Values('Describe your approach to making decisions and solving problems. Why do you do it this way',4)"];
    [db executeUpdate:@"INSERT INTO GRAD_QUESTIONS(NAME, ID_QUESTIONNAIRE) Values('When you recommend something to management, what approach do you usually use',4)"];
    [db executeUpdate:@"INSERT INTO GRAD_QUESTIONS(NAME, ID_QUESTIONNAIRE) Values('How do you assemble relevant data to make your decisions? How do you know you have enough data',4)"];
    [db executeUpdate:@"INSERT INTO GRAD_QUESTIONS(NAME, ID_QUESTIONNAIRE) Values('How much leeway do you give your employees to make decisions? How do you still maintain control?',4)"];
    
    
    [db executeUpdate:@"INSERT INTO GRAD_QUESTIONS(NAME, ID_QUESTIONNAIRE) Values('Tell me about yourself',5)"];
    [db executeUpdate:@"INSERT INTO GRAD_QUESTIONS(NAME, ID_QUESTIONNAIRE) Values('Give a brief summary of your experience inyour latest positions',5)"];
    [db executeUpdate:@"INSERT INTO GRAD_QUESTIONS(NAME, ID_QUESTIONNAIRE) Values('What are your biggest strengths',5)"];
    [db executeUpdate:@"INSERT INTO GRAD_QUESTIONS(NAME, ID_QUESTIONNAIRE) Values('What are your career goals for Underwriter',5)"];
    [db executeUpdate:@"INSERT INTO GRAD_QUESTIONS(NAME, ID_QUESTIONNAIRE) Values('What is your greatest weakness',5)"];
    
    //Insert Into Questionnaire
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd"];
    NSDate *todaysDate;
    todaysDate = [NSDate date];
    
    NSData *data = UIImagePNGRepresentation([UIImage imageNamed:@"Omnitech.png"]);
    [db executeUpdate:@"INSERT INTO GRAD_QUESTIONNAIRE(IMAGE,JOB_TITLE,JOB_DESC,COMPANY,DATE_RECEIVED,STATUS) Values(?,'Software Engineer','This position requires a candidate with strong interpersonal and self-motivational skills in order to meet deadlines. You must be a fluent communicator with both fellow team members and company clients. You must be able to apply your analytical skills to deftly troubleshoot and resolve development issues as they arise. You must be able to adapt to varied environments, projects and roles on teams.','Omnitech-Inc.',?,'Open')",data,todaysDate];

    data = UIImagePNGRepresentation([UIImage imageNamed:@"FORT.png"]);
    [db executeUpdate:@"INSERT INTO GRAD_QUESTIONNAIRE(IMAGE,JOB_TITLE,JOB_DESC,COMPANY,DATE_RECEIVED,STATUS) Values(?,'Electrical Engineer','We have a great new opportunity available for a Senior Electrical Engineer experienced in Chemical, Petrochemical, Refinery, terminal or Oil & Gas industries.   We are looking for a Senior Electrical Engineer experienced in: The design of power, lighting and grounding plans.--Producing single line diagrams and motor schematics--Sizing and specifying electrical equipment--Interpreting vendor-provided wiring document.','Fort Point',?,'Open')",data,todaysDate];

    data = UIImagePNGRepresentation([UIImage imageNamed:@"ORANGE.png"]);
    [db executeUpdate:@"INSERT INTO GRAD_QUESTIONNAIRE(IMAGE,JOB_TITLE,JOB_DESC,COMPANY,DATE_RECEIVED,STATUS) Values(?,'Decision Support Analyst','The Decision Support Analyst will be responsible for all aspects of statistical gathering and financial analysis of all significant financial business strategies for the hospital segment. This position will also be critical in the supporting role of developing the operating and capital budgets. This individual must be well organized, detail-oriented and professional with all levels of individuals in and outside the company. The ideal candidate is a team player with a positive attitude who thrives in a fast-paced environment and who has strong communication and interpersonal skills.','East Orange General Hospital',?,'Open')",data,todaysDate];

    data = UIImagePNGRepresentation([UIImage imageNamed:@"ASSOCIA.png"]);
    [db executeUpdate:@"INSERT INTO GRAD_QUESTIONNAIRE(IMAGE,JOB_TITLE,JOB_DESC,COMPANY,DATE_RECEIVED,STATUS) Values(?,'Community Office Manager','Associa-HCMS is currently accepting resumes for community association managers. Experience in the community association management industry is required, and CAI certifications are preferred. Applicants must be available for evening meetings on a routine basis. We are actively seeking a portfolio manager for HOAs in our Clear Lake office.  We are also seeking an onsite condo manager at a client location in the Medical Center area in Houston.','Houston Community Management Services',?,'Open')",data,todaysDate];
    
    [db executeUpdate:@"INSERT INTO GRAD_QUESTIONNAIRE(JOB_TITLE,JOB_DESC,COMPANY,DATE_RECEIVED,STATUS) Values('Regional Underwriter leader','The Regional Underwriting Leader partners with the Regional Sales Manager and Regional Processing Leader to support superior customer service relative to credit and underwriting activities in the region including credit escalations /interpretations and loan scenarios.','TN-Knoxville',?,'Open')",todaysDate];
    
    
    data = UIImagePNGRepresentation([UIImage imageNamed:@"network_image.png"]);
    [db executeUpdate:@"INSERT INTO GRAD_FOLLOWER(NAME,TYPE,PICTURE) Values('Robert','FAMILLY',?)",data];
    [db executeUpdate:@"INSERT INTO GRAD_FOLLOWER(NAME,TYPE,PICTURE) Values('ALI','FAMILLY',?)",data];
    [db executeUpdate:@"INSERT INTO GRAD_FOLLOWER(NAME,TYPE,PICTURE) Values('FRANCOIS','FAMILLY',?)",data];
    [db executeUpdate:@"INSERT INTO GRAD_FOLLOWER(NAME,TYPE,PICTURE) Values('Emily','FAMILLY',?)",data];
    
    [db executeUpdate:@"INSERT INTO GRAD_FOLLOWER(NAME,TYPE,PICTURE) Values('CHRIS','FOLLOWER',?)",data];
    [db executeUpdate:@"INSERT INTO GRAD_FOLLOWER(NAME,TYPE,PICTURE) Values('SALAH','FOLLOWER',?)",data];
    [db executeUpdate:@"INSERT INTO GRAD_FOLLOWER(NAME,TYPE,PICTURE) Values('ALI','FOLLOWER',?)",data];
    [db executeUpdate:@"INSERT INTO GRAD_FOLLOWER(NAME,TYPE,PICTURE) Values('Robert','FOLLOWER',?)",data];
    [db executeUpdate:@"INSERT INTO GRAD_FOLLOWER(NAME,TYPE,PICTURE) Values('FRANCOIS','FOLLOWER',?)",data];
    [db executeUpdate:@"INSERT INTO GRAD_FOLLOWER(NAME,TYPE,PICTURE) Values('Emily','FOLLOWER',?)",data];

    
    
    [db executeUpdate:@"INSERT INTO GRAD_FOLLOWER(NAME,TYPE,PICTURE) Values('Robert','FOLLOWING',?)",data];
    [db executeUpdate:@"INSERT INTO GRAD_FOLLOWER(NAME,TYPE,PICTURE) Values('ALI','FOLLOWING',?)",data];
    [db executeUpdate:@"INSERT INTO GRAD_FOLLOWER(NAME,TYPE,PICTURE) Values('FRANCOIS','FOLLOWING',?)",data];
    [db executeUpdate:@"INSERT INTO GRAD_FOLLOWER(NAME,TYPE,PICTURE) Values('Emily','FOLLOWING',?)",data];
    
    data = UIImagePNGRepresentation([UIImage imageNamed:@"apple.png"]);
    [db executeUpdate:@"INSERT INTO GRAD_FOLLOWER(NAME,TYPE,PICTURE) Values('Apple','FOLLOWING',?)",data];
    data = UIImagePNGRepresentation([UIImage imageNamed:@"google.png"]);
    [db executeUpdate:@"INSERT INTO GRAD_FOLLOWER(NAME,TYPE,PICTURE) Values('GOOGLE','FOLLOWING',?)",data];
    data = UIImagePNGRepresentation([UIImage imageNamed:@"microsoft.png"]);
    [db executeUpdate:@"INSERT INTO GRAD_FOLLOWER(NAME,TYPE,PICTURE) Values('MICROSOFT','FOLLOWING',?)",data];
    
    
    [db executeUpdate:@"INSERT INTO GRAD_TOOLBOX(CONTENT,COMPANY,DATE,TYPE) Values('Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','M.I.T',?,'English level-1')",todaysDate];
    
    [db executeUpdate:@"INSERT INTO GRAD_TOOLBOX(CONTENT,COMPANY,DATE,TYPE) Values('Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','HARVARD',?,'Mathematics 1')",todaysDate];
    
    [db executeUpdate:@"INSERT INTO GRAD_TOOLBOX(CONTENT,COMPANY,DATE,TYPE) Values('Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','ESPRIT',?,'Philosophy 1')",todaysDate];

    [db executeUpdate:@"INSERT INTO GRAD_TOOLBOX(CONTENT,COMPANY,DATE,TYPE) Values('Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','M.I.T',?,'English level-1')",todaysDate];
    
    [db executeUpdate:@"INSERT INTO GRAD_TOOLBOX(CONTENT,COMPANY,DATE,TYPE) Values('Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','HARVARD',?,'Mathematics 1')",todaysDate];
    
    [db executeUpdate:@"INSERT INTO GRAD_TOOLBOX(CONTENT,COMPANY,DATE,TYPE) Values('Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.','ESPRIT',?,'Philosophy 1')",todaysDate];

    
    [db close];
}


-(void)START //launch0-1
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"GRADUMATE.sqlite"];
    FMDatabase* db = [FMDatabase databaseWithPath:writableDBPath];
    if(![db open])
    {
        NSLog(@"Err %d: %@",[db lastErrorCode],[db lastErrorMessage]);
    }
    [db setShouldCacheStatements:YES];
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS TIMELINE (ID INTEGER PRIMARY KEY  NOT NULL, NAME TEXT);"];
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS GRAD_NBCOMMENT (ID INTEGER PRIMARY KEY  NOT NULL, ID_OBJECT INTEGER, NB_COMMENT INTEGER);"];
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS GRAD_USER (ID INTEGER PRIMARY KEY  NOT NULL, FIRSTNAME TEXT, SURNAME TEXT, EMAIL TEXT, COMP_POSITION TEXT, QUALIF TEXT, GRADUATE TEXT, FUNCTION TEXT, LOGIN TEXT, PASSWORD TEXT, VIDEO_ID INTEGER, ADDINFORMATIONS TEXT, SKYPEID TEXT, PHOTO BLOB, OK INTEGER);"];
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS GRAD_USERLIST (ID INTEGER PRIMARY KEY  NOT NULL, FIRSTNAME TEXT, SURNAME TEXT);"];
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS GRAD_CHAT (ID INTEGER PRIMARY KEY  NOT NULL, NAME TEXT, ID_USER INTEGER, TEXT TEXT, DATE DATE);"];
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS GRAD_COMMENT (ID INTEGER PRIMARY KEY  NOT NULL,TEXT TEXT, ID_ITEM INTEGER, ID_USER INTEGER, NAME TEXT, DATE DATE, FOREIGN KEY(ID_ITEM) REFERENCES GRAD_FEED(ID))"];
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS GRAD_LINK (ID INTEGER PRIMARY KEY  NOT NULL, LINK TEXT, OBJECT_ID INTEGER,FOREIGN KEY(OBJECT_ID) REFERENCES GRAD_TIMELINE(ID))"];
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS GRAD_TOOLBOX (ID INTEGER PRIMARY KEY  NOT NULL, CONTENT TEXT,COMPANY TEXT, DATE DATE, TYPE TEXT)"];
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS GRAD_TIMELINE (ID INTEGER PRIMARY KEY  NOT NULL, NAME TEXT, DESC TEXT, AchDATE DATE, TYPE TEXT, KIND TEXT, IsCompleted INTEGER, LINK TEXT, IsAccepted INTEGER, IsHidden INTEGER, IsPublished INTEGER, NBCOMMENT INTEGER, TLID INTEGER,STARTDATE DATE, ONTL INTEGER,IDU INTEGER, PID INTEGER)"];
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS GRAD_FEED (ID INTEGER PRIMARY KEY  NOT NULL, NAME TEXT, DESC TEXT, AchDATE DATE, TYPE TEXT, KIND TEXT, IsCompleted INTEGER, LINK TEXT, IsAccepted INTEGER, IsHidden INTEGER, IsPublished INTEGER, NBCOMMENT INTEGER, TLID INTEGER, STARTDATE DATE)"];
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS GRAD_REQUEST (ID INTEGER PRIMARY KEY  NOT NULL, QUESTION_ID INTEGER, COMPANY_ID INTEGER, USER_ID INTEGER, JOB_ID INTEGER, STATUES TEXT, FOREIGN KEY(QUESTION_ID) REFERENCES GRAD_QUESTIONSET(ID),FOREIGN KEY(COMPANY_ID) REFERENCES GRAD_Company(ID), FOREIGN KEY(USER_ID) REFERENCES GRAD_USER(ID), FOREIGN KEY(JOB_ID) REFERENCES GRAD_JOBDESC(ID))"];
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS GRAD_QUESTIONSET (ID INTEGER PRIMARY KEY  NOT NULL, COMPANY_ID INTEGER, JOBDESC_ID INTEGER, FOREIGN KEY(COMPANY_ID) REFERENCES GRAD_Company(ID), FOREIGN KEY(JOBDESC_ID) REFERENCES GRAD_JOBDESC(ID))"];
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS GRAD_Company (ID INTEGER PRIMARY KEY  NOT NULL, COMPANY_DESC TEXT, COMPANY_ADRESS TEXT,COMPANY_TYPE TEXT,  Number_Worker INTEGER)"];
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS GRAD_USER_COMPANY (ID INTEGER PRIMARY KEY  NOT NULL, COMPANY_ID INTEGER, USER_ID INTEGER, FOREIGN KEY(COMPANY_ID) REFERENCES GRAD_Company(ID), FOREIGN KEY(USER_ID) REFERENCES GRAD_USER(ID))"];
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS GRAD_VIDEOS (ID INTEGER PRIMARY KEY  NOT NULL, USER_ID INTEGER, CONTENT TEXT, QUESTIONSET_ID INTEGER, PRIVACY TEXT, FOREIGN KEY(USER_ID) REFERENCES GRAD_USER(ID), FOREIGN KEY(QUESTIONSET_ID) REFERENCES GRAD_QUESTIONSET(ID))"];
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS GRAD_University (ID INTEGER PRIMARY KEY  NOT NULL, UNIVERSITY_NAME TEXT,STARTD DATE, GRADUATION_DATE DATE)"];
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS GRAD_CV (ID INTEGER PRIMARY KEY  NOT NULL, Photo BLOB, PRIVACY TEXT, USER_ID INTEGER, FOREIGN KEY(USER_ID) REFERENCES GRAD_USER(ID))"];
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS GRAD_ADDRESS (ID INTEGER PRIMARY KEY  NOT NULL, USER_ID INTEGER,STREET_ADRESS TEXT, POSTAL_CODE TEXT, CITY TEXT, COUNTRY TEXT,FOREIGN KEY(USER_ID) REFERENCES GRAD_USER(ID))"];
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS GRAD_Telephone (ID INTEGER PRIMARY KEY  NOT NULL, TYPE TEXT, NUMBER TEXT, USER_ID INTEGER, FOREIGN KEY(USER_ID) REFERENCES GRAD_USER(ID))"];
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS GRAD_Achievements (ID INTEGER PRIMARY KEY  NOT NULL, STARTD DATE, ENDD DATE, DESCRIPTION TEXT, USER_ID INTEGER, FOREIGN KEY(USER_ID) REFERENCES GRAD_USER(ID))"];
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS GRAD_USER_UNIVERSITY (ID INTEGER PRIMARY KEY  NOT NULL, USER_ID INTEGER, UNIVERSITY_ID INTEGER, FOREIGN KEY(USER_ID) REFERENCES GRAD_USER(ID), FOREIGN KEY(UNIVERSITY_ID) REFERENCES GRAD_University(ID))"];
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS GRAD_LANGUAGE (ID INTEGER PRIMARY KEY  NOT NULL, LANGUAGE TEXT, VIDEO TEXT)"];
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS GRAD_JOBDESC (ID INTEGER PRIMARY KEY  NOT NULL, TITLE TEXT, DESCRIPTION TEXT, Start_Date DATE, End_Date DATE, STATE INTEGER, USER_ID INTEGER, COMPANY_ID INTEGER,  QUESTIONS_ID INTEGER, CITY TEXT, COUNTRY TEXT, COMPANY_NAME TEXT, PRIVACY TEXT, FOREIGN KEY(USER_ID) REFERENCES GRAD_USER(ID),FOREIGN KEY(COMPANY_ID) REFERENCES GRAD_Company(ID),FOREIGN KEY(QUESTIONS_ID) REFERENCES GRAD_QUESTIONSET(ID))"];
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS GRAD_Reference (ID INTEGER PRIMARY KEY  NOT NULL, WRITE DATE, Refer_ID INTEGER,  Reference_ID INTEGER, DESCR TEXT, LINK TEXT , NAME TEXT, PRIVACY TEXT, FOREIGN KEY(Refer_ID) REFERENCES GRAD_USER(ID), FOREIGN KEY(Reference_ID) REFERENCES GRAD_USER(ID))"];
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS GRAD_SKILLS (ID INTEGER PRIMARY KEY  NOT NULL, USER_ID INTEGER,NAME TEXT, PRIVACY TEXT, FOREIGN KEY(USER_ID) REFERENCES GRAD_USER(ID))"];
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS GRAD_EDUTR (ID INTEGER PRIMARY KEY  NOT NULL, USER_ID INTEGER, FUNCTION INTEGER, ESTABLISHMENT TEXT, GRADE TEXT, DATEACH DATE, TYPE TEXT, PRIVACY TEXT, FOREIGN KEY(USER_ID) REFERENCES GRAD_USER(ID))"];
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS GRAD_ReferenceLink (ID INTEGER PRIMARY KEY  NOT NULL, LINK TEXT, NAME TEXT)"];
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS GRAD_QUESTIONNAIRE (ID INTEGER PRIMARY KEY  NOT NULL, IMAGE BLOB, JOB_TITLE TEXT, JOB_DESC TEXT, COMPANY TEXT, DATE_RECEIVED DATE, STATUS TEXT, ID_LANGAGE INTEGER,DATE_APPlIED DATE, FOREIGN KEY(ID_LANGAGE) REFERENCES GRAD_LANGUAGE(ID))"];
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS GRAD_QUESTIONS (ID INTEGER PRIMARY KEY  NOT NULL, NAME TEXT,ID_QUESTIONNAIRE INTEGER, FOREIGN KEY(ID_QUESTIONNAIRE) REFERENCES GRAD_QUESTIONNAIRE(ID))"];
    
    [db executeUpdate:@"CREATE TABLE IF NOT EXISTS GRAD_FOLLOWER (ID INTEGER PRIMARY KEY  NOT NULL, NAME TEXT,TYPE TEXT, PICTURE BLOB, ID_USER INTEGER, FOREIGN KEY(ID_USER) REFERENCES GRAD_USER(ID))"];
    
    [self Insert];
    
    
    [db close];
    
}


-(void)forget
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"GRADUMATE.sqlite"];
    FMDatabase* db = [FMDatabase databaseWithPath:writableDBPath];
    if(![db open])
    {
        NSLog(@"Err %d: %@",[db lastErrorCode],[db lastErrorMessage]);
    }
    [db setShouldCacheStatements:YES];
    [db executeUpdate:@"DROP TABLE TIMELINE;"];
    [db executeUpdate:@"DROP TABLE GRAD_NBCOMMENT;"];
    [db executeUpdate:@"DROP TABLE GRAD_USER ;"];
    [db executeUpdate:@"DROP TABLE GRAD_USERLIST;"];
    [db executeUpdate:@"DROP TABLE  GRAD_CHAT;"];
    [db executeUpdate:@"DROP TABLE  GRAD_COMMENT;"];
    [db executeUpdate:@"DROP TABLE  GRAD_LINK;"];
    [db executeUpdate:@"DROP TABLE GRAD_TOOLBOX;"];
    
//    [db executeUpdate:@"DROP TABLE GRAD_TIMELINE;"];
  
    [db executeUpdate:@"DROP TABLE GRAD_FEED;"];
    [db executeUpdate:@"DROP TABLE GRAD_REQUEST;"];
    [db executeUpdate:@"DROP TABLE GRAD_QUESTIONSET;"];
    [db executeUpdate:@"DROP TABLE GRAD_Company;"];
    [db executeUpdate:@"DROP TABLE GRAD_USER_COMPANY;"];
    [db executeUpdate:@"DROP TABLE GRAD_VIDEOS;"];
    [db executeUpdate:@"DROP TABLE GRAD_University;"];
    [db executeUpdate:@"DROP TABLE GRAD_CV;"];
    [db executeUpdate:@"DROP TABLE GRAD_ADDRESS;"];
    [db executeUpdate:@"DROP TABLE  GRAD_Telephone;"];
    [db executeUpdate:@"DROP TABLE  GRAD_Achievements;"];
    [db executeUpdate:@"DROP TABLE  GRAD_USER_UNIVERSITY;"];
    [db executeUpdate:@"DROP TABLE  GRAD_LANGUAGE;"];
    [db executeUpdate:@"DROP TABLE  GRAD_JOBDESC;"];
    [db executeUpdate:@"DROP TABLE  GRAD_Reference;"];
    [db executeUpdate:@"DROP TABLE  GRAD_SKILLS;"];
    [db executeUpdate:@"DROP TABLE  GRAD_EDUTR;"];
    [db executeUpdate:@"DROP TABLE  GRAD_ReferenceLink;"];
    [db executeUpdate:@"DROP TABLE  GRAD_QUESTIONNAIRE;"];
    [db executeUpdate:@"DROP TABLE  GRAD_QUESTIONS;"];
    [db executeUpdate:@"DROP TABLE  GRAD_FOLLOWER;"];
    [self START];
    [self Insert];
    
    [db close];
    
}


@end

//
//  AddBubbleViewController.m
//  GRADUMATE
//
//  Created by Jarray on 10/11/2015.
//  Copyright © 2015 SlickStone. All rights reserved.
//

#import "AddBubbleViewController.h"
#import <AFNetworking/AFHTTPRequestOperationManager.h>
#import "iCarousel.h"
#import <UIKit/UIKit.h>
#import "FMDatabase.h"
#import "JSONHelper.h"
#import "AppDelegate.h"
#import "Singleton.h"
#import "BubbleLinkedCell.h"
#import "Bubble.h"
#import "BubbleView.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <JDFTooltips/JDFTooltipView.h>
#import <ANProgressStepper/ANProgressStepper.h>
@interface AddBubbleViewController (){
    int firstCreation;
    NSString *NameBubble;
    NSString *DescriptionBubble;
    NSString *BubbleKind;
    BOOL BubbleIsAcheived;
    BOOL ToolTipHidden;
    NSMutableArray*unlinkedBubble;
    NSMutableArray*indexes,*selectedBubbles;
    Bubble*myBubble;
    JDFTooltipView *tooltip;
    NSString*imageName;
}

@end

@implementation AddBubbleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    ToolTipHidden=YES;
     selectedBubbles=[NSMutableArray array];
    bubbleDescriptionView.frame=self.view.bounds;
    [self.view addSubview:bubbleDescriptionView];
    
//    stepper.currentStep = 2;
    
    date.tintColor = [UIColor clearColor];
    myBubble=[[Bubble alloc]init];
    
    // Do any additional setup after loading the view.
    
    carouselView.type = iCarouselTypeWheel;
    carouselView.delegate = self;
    carouselView.dataSource = self;
    
//    [self addInfoContainerView];
    unlinkedBubble=[[Singleton sharedInstance] BubblesWithParentID:[NSNumber numberWithInt:0]];
    
    switch (_BubbleType)
    {
        case 1:
            BubbleKind = @"GOAL";
            break;
        case 2:
            BubbleKind = @"OBJECTIVE";
            break;
        case 3:
            BubbleKind = @"CERTIFICATE";
            break;
        case 4:
            BubbleKind = @"COURSE";
            break;
        case 5:
            BubbleKind = @"EVENT";
            break;
        case 6:
            BubbleKind = @"ACTIVITY";
            
            break;
        default:
            break;
    }
    myBubble.KIND=BubbleKind;
//    
//    [self InitNavBar];
    [self InitController];
}

//-(void)addInfoContainerView{
//    [self.view addSubview:InfoContainerView];
////    [InfoContainerView setTranslatesAutoresizingMaskIntoConstraints:NO];
//
//    
//    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:InfoContainerView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0 constant:0]];
//    
//    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:InfoContainerView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0]];
//    
//    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:InfoContainerView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0]];
//    
//    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:InfoContainerView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0]];
//    
//    [InfoContainerView layoutIfNeeded];
//    
//}
- (void)InitNavBar
{
    UIButton *SaveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [SaveBtn setTitle:@"Ok" forState:UIControlStateNormal];
    SaveBtn.frame = CGRectMake(0,0,40,40);
    [SaveBtn addTarget:self action:@selector(SaveBubble) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *okBtn =[[UIBarButtonItem alloc] initWithCustomView:SaveBtn];
    self.navigationItem.rightBarButtonItem=okBtn;
}

- (void)InitController
{
    //[iconBtn setBackgroundImage:[self GetDefaultIconForBubble] forState:UIControlStateNormal];
//    
//    NSDate *NowDate=[NSDate date];
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"dd MMMM yyyy"];
//    NSString *toDay=[[dateFormatter stringFromDate:NowDate] uppercaseString];
////    DateLab.text=[NSString stringWithFormat:@"%@ %@ %@",[toDay substringWithRange:NSMakeRange(0, 2)],[toDay substringWithRange:NSMakeRange(3, 3)],[toDay substringWithRange:NSMakeRange(toDay.length-4, 4)]];
//    
////    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:DateLab.text];
////    [attributedString addAttribute:NSFontAttributeName
////                  value:[UIFont systemFontOfSize:24.0]
////                  range:NSMakeRange(0, 2)];
////    
////    [attributedString addAttribute:NSFontAttributeName
////                             value:[UIFont systemFontOfSize:24.0]
////                             range:NSMakeRange(DateLab.text.length-4, 4)];
////    
////    [DateLab setAttributedText:attributedString];
//    
//    Datepicker.minimumDate = [[ NSDate alloc ] initWithTimeIntervalSinceNow: (NSTimeInterval) 0 ];
//    Datepicker.date=NowDate;
////    Datepicker.backgroundColor = [UIColor groupTableViewBackgroundColor];
//    
//    BubbleIsAcheived=false;
    

//    firstCreation=0;
    
//    [InfoView setClipsToBounds:YES];
//    InfoView.layer.cornerRadius = 5;
    
    chooseImage.image=[self GetDefaultIconForBubble];
    
    [BubbleDescription setClipsToBounds:YES];
    BubbleDescription.layer.cornerRadius = 5;
    BubbleDescription.textColor=[UIColor lightGrayColor];
    
//    InfoContainerView.hidden=false;
//    [[[UIApplication sharedApplication] keyWindow].rootViewController.view addSubview:InfoContainerView];
    
}

- (void)viewWillAppear:(BOOL)animated {
//    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillAppear:animated];
}

-(IBAction)ShowInfo:(UIButton*)sender
{
    if(firstCreation==0)
    {
        BubbleName.text=@"";
        BubbleDescription.text=@"";
    }
}

-(IBAction)IsAcheived:(UIButton*)sender
{
    if(sender.tag==0)
    {
        BubbleIsAcheived=true;
        [sender setBackgroundImage:[UIImage imageNamed:@"NewBubble_Check1"] forState:UIControlStateNormal];
        sender.tag=1;
    }
    else
    {
        BubbleIsAcheived=false;
        [sender setBackgroundImage:[UIImage imageNamed:@"NewBubble_Check0"] forState:UIControlStateNormal];
        sender.tag=0;
    }
}

-(IBAction)ShowBubbleFiles:(UIButton*)sender
{
    
}

-(IBAction)CreateSubBubble
{
    
}

-(IBAction)ChangedDate
{
    
    [date resignFirstResponder];
    NSDate *NowDate=Datepicker.date;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMM yy"];
    NSString *toDay=[[dateFormatter stringFromDate:NowDate] uppercaseString];
    NSRange boldedRange = NSMakeRange(3, toDay.length-6);
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:toDay];
   
    [attrString addAttribute:NSFontAttributeName
                                  value:[UIFont systemFontOfSize:30.0]
                                  range:boldedRange];

   
    [date setAttributedText:attrString];
    myBubble.bubbleDate=Datepicker.date;
    
}

-(IBAction)SelectIcon:(UITapGestureRecognizer*)sender
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.accessibilityHint=@"1";
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
}

#pragma mark - Image Picker Controller delegate methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    NSURL *imagePath = [info objectForKey:@"UIImagePickerControllerReferenceURL"];

    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *param in [imagePath.query componentsSeparatedByString:@"&"]) {
        NSArray *elts = [param componentsSeparatedByString:@"="];
        if([elts count] < 2) continue;
        [params setObject:[elts objectAtIndex:1] forKey:[elts objectAtIndex:0]];
    }

    chooseImage.image=chosenImage;
    myBubble.image=[NSString stringWithFormat:@"%@.%@",[params objectForKey:@"id"],[params objectForKey:@"ext"]];;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    [chooseImage setClipsToBounds:YES];
    chooseImage.layer.cornerRadius = chooseImage.frame.size.width/2;
    chooseImage.layer.borderColor = [UIColor colorWithRed:0xFF/255.f green:0xE1/255.f blue:0xB2/255.f alpha:1].CGColor;
    chooseImage.layer.borderWidth =8;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)SaveBubble
{
    if(firstCreation==1)
        [self SaveBubbleToServer:@"true" parentID:@"-1"];
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"New bubble" message:@"Please insert a name for the bubble" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (BOOL)CanBeParent
{
    BOOL BeParent = false;
    
    switch (_BubbleType)
    {
        case 1:
            BeParent=true;
            break;
        case 2:
            BeParent=true;
            break;
        case 3:
            BeParent=true;
            break;
        case 4:
            BeParent=true;
            break;
        case 5:
            BeParent=true;
            break;
        case 6:
            BeParent=false;
            break;
    }
    
    return BeParent;
}

- (void)SaveBubbleToServer:(NSString*)IsParent parentID:(NSString*)parentID
{
 
}

#pragma mark UITableview Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [unlinkedBubble count];
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    BubbleLinkedCell*cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.checked.alpha=0;
    
    NSLog(@"indexes %@",indexes);
    if ([indexes containsObject:[NSString stringWithFormat:@"%d",indexPath.row]]) {
        cell.checked.alpha=1;
    }
    [cell initCell];
    cell.cellTitle.text=((Bubble*)[unlinkedBubble objectAtIndex:indexPath.row]).NAME;

    return cell;
}

#pragma mark UITableview Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    BubbleLinkedCell*cell=[tableView cellForRowAtIndexPath:indexPath];
    
    Bubble*b=((Bubble*)[unlinkedBubble objectAtIndex:indexPath.row]);
    
    if (!indexes) {
        indexes=[NSMutableArray array];
    }

    if ([indexes containsObject:[NSString stringWithFormat:@"%d",indexPath.row]]) {
        [indexes removeObject:[NSString stringWithFormat:@"%d",indexPath.row]];
        [selectedBubbles removeObject:b];
        cell.checked.alpha=0;
    }else{
        [indexes addObject:[NSString stringWithFormat:@"%d",indexPath.row]];
        [selectedBubbles addObject:b];
        cell.checked.alpha=1;
    }
    
    [carouselView reloadData];
}

#pragma mark Actions

-(IBAction)addBubble:(id)sender{
    
    self.internetReachability = [Reachability reachabilityForInternetConnection];
    [self.internetReachability startNotifier];
    NetworkStatus netStatus = [self.internetReachability currentReachabilityStatus];
    
    myBubble.image=imageName;
    if (date.text.length==0) {
        [[[UIAlertView alloc]initWithTitle:@"" message:@"Please select date to create a new bubble."  delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
    }else if (netStatus==NotReachable){
        
        NSString *userid=[[[NSUserDefaults standardUserDefaults] objectForKey:@"GRAD_USER"] objectForKey:@"ID"];
        NSString*key=[NSString stringWithFormat:@"non_synchronised_bubbles_%@",userid];
        NSMutableArray*bubblesOffline=[[[NSUserDefaults standardUserDefaults]objectForKey:key] mutableCopy];
        if (!bubblesOffline) {
            bubblesOffline=[[NSMutableArray alloc]init];
        }
        
        [bubblesOffline addObject:[NSKeyedArchiver archivedDataWithRootObject:myBubble]];
//        NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:bubblesOffline];
        [[NSUserDefaults standardUserDefaults] setObject:bubblesOffline forKey:key];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
         [[[UIAlertView alloc] initWithTitle:@"New bubble" message:@"new bubble succuessfully created!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
         [self.navigationController popViewControllerAnimated:YES];
        
    }else{
        [JSONHelper saveBubble:myBubble withImage:chooseImage.image delegate:self tag:0];
    }
}
-(void)receiveData:(id)responseData tag:(int)tag{
    
    myBubble.ID=[NSNumber numberWithInt:[[responseData valueForKey:@"ID"] intValue]];
    for (Bubble*b in selectedBubbles) {
        b.PID=myBubble.ID;
        
        [[Singleton sharedInstance] UpdateParentId:b.PID withID:b.ID];
    }
    [[Singleton sharedInstance] saveBubble:myBubble withID:myBubble.ID];
    [self.navigationController popViewControllerAnimated:YES];
}
    
-(IBAction)SelectDate
{
    
    if(Datepicker.tag==0)
    {
        Datepicker.hidden=false;
        Datepicker.tag=1;
    }
    else
    {
        Datepicker.hidden=true;
        Datepicker.tag=0;
    }
}

#pragma mark bubble description view
-(IBAction)Infoback
{
//    [bubbleDescriptionView removeFromSuperview];
    [self.navigationController popViewControllerAnimated:NO];
}
-(IBAction)InfoNext
{
    
    if (BubbleName.text.length==0||BubbleDescription.text.length==0) {
        [[[UIAlertView alloc]initWithTitle:@"New Bubble" message:@"Please insert a name and a description for the bubble" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
    }else{
//        [bubbleDescriptionView removeFromSuperview];
         firstCreation=1;
        nameOfBubble.text=[NSString stringWithFormat:@"%@:%@",BubbleName.text,myBubble.KIND];
        myBubble.NAME=BubbleName.text;
        myBubble.DESC=BubbleDescription.text;
        BubbleLinkedView.frame=self.view.bounds;
        [self.view addSubview:BubbleLinkedView];
    }
}
#pragma mark link my bubble view

-(IBAction)BubbleNotLinked{
    [bubbleDescriptionView removeFromSuperview];
     [BubbleLinkedView removeFromSuperview];
    
}
-(IBAction)BubbleLinked{
    [BubbleLinkedView removeFromSuperview];
    bubblesListView.frame=self.view.bounds;
    [self.view addSubview:bubblesListView];
    
}

#pragma mark Inlinked bubbles view
-(IBAction)LinkMyBubble{
    
    if([selectedBubbles count]==0){
        [[[UIAlertView alloc]initWithTitle:@"" message:@"You should select at least one bubble!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }else{
        [bubblesListView removeFromSuperview];
        [bubbleDescriptionView removeFromSuperview];
        [carouselView reloadData];
    }
}

-(IBAction)EscapeFromLinking{
    
    indexes=nil;
    [bubblesListView removeFromSuperview];
}

-(IBAction)ShowDescription{
    
        if (!tooltip) {
            tooltip = [[JDFTooltipView alloc] initWithTargetView:descriptBtn hostView:self.view tooltipText:BubbleDescription.text arrowDirection:JDFTooltipViewArrowDirectionUp width:200.0f];
            tooltip.dismissOnTouch=YES;

         [tooltip show];
        }else{
        [tooltip hideAnimated:YES] ;
            tooltip=nil;
        }
}

-(IBAction)ShowLinkedBubbles{
    
    bubblesListView.frame=self.view.bounds;
    [self.view addSubview:bubblesListView];
    for(int i=0;i<[indexes count]; i++){
        int indeRow=[[indexes objectAtIndex:i] intValue];
        NSIndexPath*ind=[NSIndexPath indexPathForRow:indeRow inSection:0];
        BubbleLinkedCell*cell=[table cellForRowAtIndexPath:ind];
        cell.checked.alpha=1;
    }

}

-(IBAction)Back:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSString *)GetUserID
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"GRADUMATE.sqlite"];
    
    long MyUserID=-1;
    FMDatabase* db = [FMDatabase databaseWithPath:writableDBPath];
    if(![db open])
    {
        NSLog(@"Opening DB Error %d: %@",[db lastErrorCode],[db lastErrorMessage]);
    }
    else
    {
        [db setShouldCacheStatements:YES];
        FMResultSet *R1 = [db executeQuery:@"SELECT* FROM GRAD_USER"];
        while ([R1 next])
        {
            MyUserID = (long)[R1 intForColumn:@"ID"];
        }
    }
    
    return [NSString stringWithFormat:@"%ld",MyUserID];
}

//- (void)keyboardWillShow:(NSNotification *)notification
//{
//    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
//    int keyboardHeight = MIN(keyboardSize.height,keyboardSize.width);
//    keyboardHeight=keyboardHeight-70;
//    
//    [UIView animateWithDuration:.2f animations:^{
//        CGRect theFrame = CGRectMake(0,-keyboardHeight,[[UIScreen mainScreen] bounds].size.width,[[UIScreen mainScreen] bounds].size.height);
//        InfoContainerView.frame = theFrame;
//    }];
//}

- (void)keyboardWillShow:(NSNotification*)aNotification
{
    
    if (BubbleDescription) {
        NSDictionary* info = [aNotification userInfo];
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
        DESC_SCROLL.contentInset = contentInsets;
        DESC_SCROLL.scrollIndicatorInsets = contentInsets;
        
        CGRect fr=BubbleDescription.frame;
        fr.size.height=fr.size.height/2;
        BubbleDescription.frame=fr;
        
        [DESC_SCROLL scrollRectToVisible:BubbleDescription.frame animated:YES];
    }
}

- (void)keyboardWillHide:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    DESC_SCROLL.contentInset = contentInsets;
    DESC_SCROLL.scrollIndicatorInsets = contentInsets;
}
-(UIImage*) GetDefaultIconForBubble
{

    switch (_BubbleType)
    {
        case 1:
            imageName=@"TLgoal";
            break;
        case 2:
            imageName=@"TLobjective";
            break;
        case 3:
            imageName=@"TLcertificate";
            break;
        case 4:
            imageName=@"TLcourse";
            break;
        case 5:
            imageName=@"TLevent";
            break;
        case 6:
            imageName=@"TLactivity";
            break;
    }
    
    UIImage *icon=[UIImage imageNamed:imageName];
    return icon;
}

//-(void)keyboardWillHide:(NSNotification *)notification
//{
//    [UIView animateWithDuration:.2f animations:^{
//        CGRect theFrame = CGRectMake(0,0,[[UIScreen mainScreen] bounds].size.width,[[UIScreen mainScreen] bounds].size.height);
//        InfoContainerView.frame = theFrame;
//    }];
//}

#pragma mark - UITextView Delegate
//- (void)textViewDidBeginEditing:(UITextView *)textView
//{
//    if ([textView.text isEqualToString:@"add your description here..."])
//    {
//        textView.text = @"";
//        textView.textColor = [UIColor blackColor];
//    }
//    [textView becomeFirstResponder];
//}
//
//- (BOOL)textViewShouldEndEditing:(UITextView *)textView
//{
////    if ([textView.text isEqualToString:@""]) {
////        textView.text = @"add your description here...";
////        textView.textColor = [UIColor lightGrayColor];
////    }
//    [BubbleDescription resignFirstResponder];
//    return YES;
//}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    UIToolbar * keyboardToolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
    
    keyboardToolBar.barStyle = UIBarStyleDefault;
    [keyboardToolBar setItems: [NSArray arrayWithObjects:
                                [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(resignKeyboard)],
                                nil]];
    textView.inputAccessoryView = keyboardToolBar;
    
    return YES;
    
}

-(void)resignKeyboard{
    [BubbleDescription resignFirstResponder];
}


- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (range.length == 0) {
        if ([text isEqualToString:@"\n"]) {
            textView.text = [NSString stringWithFormat:@"%@\n",textView.text];
            return NO;
        }
    }
    
    return YES;
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField==date) {
        UIToolbar*keyboardToolbar;
        if (keyboardToolbar == nil) {
            keyboardToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
            [keyboardToolbar setBarStyle:UIBarStyleDefault];
            
            UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
            
            UIBarButtonItem *accept = [[UIBarButtonItem alloc] initWithTitle:@"Ok" style:UIBarButtonItemStyleDone target:self action:@selector(ChangedDate)];
            
            [keyboardToolbar setItems:[[NSArray alloc] initWithObjects: extraSpace, accept, nil]];
        }
        textField.inputAccessoryView = keyboardToolbar;
        Datepicker = [[UIDatePicker alloc] init] ;
        CGRect fr=Datepicker.frame;
        fr.size.height=150;
        Datepicker.frame=fr;
        Datepicker.datePickerMode = UIDatePickerModeDate;
        //    [Datepicker addTarget:self action:@selector(ChangedDate) forControlEvents:UIControlEventValueChanged];
        textField.inputView=Datepicker;
        
        NSDate *NowDate=[NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd MMMM yy"];
        //    NSString *toDay=[[dateFormatter stringFromDate:NowDate] uppercaseString];
        Datepicker.date=NowDate;
    }
    
    return YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark iCarousel methods

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    //return the total number of items in the carousel
    if ([selectedBubbles count]<3) {
        return 3;
    }
    else
        return [selectedBubbles count]+1;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    UILabel *label = nil;
    
    //create new view if no view is available for recycling
    if (view == nil)
    {
       view= [[[NSBundle mainBundle] loadNibNamed:@"bubbleView" owner:self options:nil] firstObject];
        UIImageView*bubbleImage=((BubbleView*)view).bubbleImage;
        if (index<[selectedBubbles count]) {
            [bubbleImage sd_setImageWithURL:[NSURL URLWithString:((Bubble*)[selectedBubbles objectAtIndex:index]).image] placeholderImage:[UIImage imageNamed:@"TLcourse"]];
            bubbleImage.layer.masksToBounds=YES;
        }else
            bubbleImage.image=[UIImage imageNamed:@"NewBubble_Add"];
        
        ((BubbleView*)view).bubbleImage.layer.cornerRadius=bubbleImage.frame.size.width/2;

        view.frame=CGRectMake(0, 0, 200, 300);
    }
    else
    {
        //get a reference to the label in the recycled view
        label = (UILabel *)[view viewWithTag:1];
    }
    
    //set item label
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {...}` check otherwise
    //you'll get weird issues with carousel item content appearing
    //in the wrong place in the carousel
//    label.text = [_items[index] stringValue];
    
    return view;
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    if (option == iCarouselOptionSpacing)
    {
        return value * 1.1;
    }
    return value;
}


//#pragma mark Stepper
//
//- (NSUInteger)numberOfSteps
//{
//    return 2;
//}
//
//- (CGFloat)lineThickness
//{
//    return 2.0f;
//}
//
//- (UIColor *)incompleteColor
//{
//    return [UIColor whiteColor];
//}
//
//- (UIColor *)completeColor
//{
//    return [UIColor whiteColor];
//}
//
//- (UIColor *)activeColor
//{
//    return [UIColor clearColor];
//}
//
//- (UIColor *)incompleteNumberColor
//{
//    return [UIColor colorWithRed:0/255.f green:144/255.f blue:210/255.f alpha:1];
//}
//
//- (UIColor *)completeNumberColor
//{
//     return [UIColor colorWithRed:0/255.f green:144/255.f blue:210/255.f alpha:1];
//}
//
//- (UIColor *)activeNumberColor
//{
//    return [UIColor whiteColor];
//}
//
//- (UIColor *)borderColor
//{
//    return [UIColor whiteColor];
//}
//
//- (CGFloat)numberSize
//{
//    return 12.0f;
//}
//
//- (BOOL)showLinesBetweenSteps
//{
//    return YES;
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  MenuViewController.m
//  GRADUMATE
//
//  Created by Ahmad Jarray on 26/11/2015.
//  Copyright © 2015 Ahmad Jarray. All rights reserved.
//

#import "MenuViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "MenuCell.h"
#import "CropVC/CropViewController.h"

@interface MenuViewController (){
    NSArray *titres,*images,*notifications;
    NSString*imageName;;
}

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    titres =@[@"Live Line",@"Feeds",@"Community"];
    images=@[[UIImage imageNamed:@"dashboard-icon"],[UIImage imageNamed:@"notification-icon"],[UIImage imageNamed:@"community-icon"]];
    notifications=@[@"0",@"4",@"0"];
    
    table.estimatedRowHeight = 150.0;
    NSDictionary*user=[[NSUserDefaults standardUserDefaults] objectForKey:@"GRAD_USER"];
    name.text=[NSString stringWithFormat:@"%@ %@",[user objectForKey:@"NAME"],[user objectForKey:@"SURNAME"]];
    
    NSString* userid=[user objectForKey:@"LOGIN"] ;
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@.png",@IMAGE_BASE_URL,userid]];
    [userImage sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"58"]];
    userImage.layer.masksToBounds=YES;
    userImage.layer.cornerRadius=userImage.frame.size.height/2;
    userImage.layer.borderWidth=4;
    userImage.layer.borderColor=[PRINCIPAL_BORDER_COLOR CGColor];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)changeImage:(id)sender{
    
    UIActionSheet*  actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"library", nil),NSLocalizedString(@"camera", nil),nil];
    actionSheet.actionSheetStyle = UIBarStyleBlack;
    [actionSheet showFromRect:CGRectMake(0,300, 200,200) inView:self.view animated:YES];

}

#pragma mark -  UIActionSheet delegate

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0:
        {
            UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
            imagePickerController.delegate = self;
            [self presentViewController:imagePickerController animated:YES completion:nil];
        }
            
            break;
        case 1:
        {
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
            {
                UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
                [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
                [imagePicker setDelegate:self];
                [self presentViewController:imagePicker animated:YES completion:nil];
            }
        }
            break;
            
        default:
            break;
    }
}

//-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
//    switch (buttonIndex)
//    {
//        case 0:
//        {
//            NSLog(@"0");
//        }
//            break;
//        case 1:
//            NSLog(@"1");
//            break;
//        case 2:
//            NSLog(@"2");
//            break;
//        case 3:
//        {
//            NSLog(@"3");
//            UIViewController* CV = [self.storyboard instantiateViewControllerWithIdentifier:@"CV"];
//            [self.navigationController pushViewController:CV animated:YES];
//            
//        }
//            break;
//        case 4:
//        {
//            NSLog(@"4");
//            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
//            picker.delegate = self;
//            picker.accessibilityHint=@"1";
//            picker.allowsEditing = YES;
//            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//            [self presentViewController:picker animated:YES completion:NULL];
//        }
//            break;
//        case 5:
//        {
//            UIViewController* set = [self.storyboard instantiateViewControllerWithIdentifier:@"SETTING"];
//            set.modalPresentationStyle = UIModalPresentationCurrentContext;
//            set.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
//            [self presentViewController:set animated:YES completion:nil];
//        }
//        default:
//            break;
//    }
//}

#pragma mark -  imagePickerController delegate
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    CropViewController*cropVC=[self.storyboard instantiateViewControllerWithIdentifier:@"CropVC"];
    UIImage* image = [info valueForKey:UIImagePickerControllerOriginalImage];
    //    _awak NSString *fileName1 ;
    NSURL *imagePath = [info objectForKey:@"UIImagePickerControllerReferenceURL"];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *param in [imagePath.query componentsSeparatedByString:@"&"]) {
        NSArray *elts = [param componentsSeparatedByString:@"="];
        if([elts count] < 2) continue;
        [params setObject:[elts objectAtIndex:1] forKey:[elts objectAtIndex:0]];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [[self navigationController] pushViewController:cropVC animated:YES];
    cropVC.delegate=self;

    cropVC.imageName=[NSString stringWithFormat:@"%@.%@",[params objectForKey:@"id"],[params objectForKey:@"ext"]];
    imageName=cropVC.imageName;
    cropVC.image = image;
}

-(void)setImage:(UIImage*)image{
    
    userImage.image=image;
    
}

- (void)ImageCropViewController:(ImageCropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage{
    //    image = croppedImage;
    userImage.image = croppedImage;
    [self.navigationController popToRootViewControllerAnimated:YES];
    //    [self sendimage];
}
//- (void)ImageCropViewControllerDidCancel:(ImageCropViewController *)controller{
//    imagechosen.image = self.image;
//    [self.navigationController popViewControllerAnimated:YES];
//}
- (IBAction)Logout:(id)sender
{
    [[Singleton sharedInstance] logout];
}

-(IBAction)Feed:(id)sender
{
    UITabBarController*feed=[self.storyboard instantiateViewControllerWithIdentifier:@"feed"];
    [[Singleton sharedInstance].drawerController setCenterViewController:feed
                                                      withCloseAnimation:YES
                                                              completion:nil];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MenuCell*cell = [tableView dequeueReusableCellWithIdentifier:@"menuCell"];
    
    [cell configWithImage:[images objectAtIndex:indexPath.row] title:[titres objectAtIndex:indexPath.row] notification:[notifications objectAtIndex:indexPath.row]];
//
    cell.contentView.backgroundColor=[UIColor colorWithWhite:1 alpha:0.3/(indexPath.row+1)];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
        return   UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UIViewController *TL;
    
    switch (indexPath.row) {
        case 0:
              TL = [self.storyboard instantiateViewControllerWithIdentifier:@"TLNAV1"];
            break;
        case 1:
             TL= [self.storyboard instantiateViewControllerWithIdentifier:@"feed"];
            
            break;
        case 2:
             TL= [self.storyboard instantiateViewControllerWithIdentifier:@"networkNav"];
            
            break;
            
        default:
            break;
    }
    
    [[Singleton sharedInstance].drawerController setCenterViewController:TL
                                                      withCloseAnimation:YES
                                                              completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  CommentViewController.h
//  GRADUMATE
//
//  Created by Ahmad Jarray on 04/12/2015.
//  Copyright © 2015 Ahmad Jarray. All rights reserved.
//

@class SOCIALFEED;
@interface CommentViewController : UIViewController{
    IBOutlet UIImageView *bubbleImage;
    IBOutlet UILabel *bubbleTitle,*bubbleComment;
    IBOutlet UITableView *table;
    IBOutlet UITextField *commentLabel;
    IBOutlet NSLayoutConstraint *keyboardConst;
    
}

@property(nonatomic,retain)SOCIALFEED * feed;

@end

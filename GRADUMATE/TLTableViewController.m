//
//  TLTableViewController.m
//  GRADUMATE
//
//  Created by Malek Mansour on 6/22/15.
//  Copyright (c) 2015 SlickStone. All rights reserved.
//

#import "TLTableViewController.h"
#import "FMDatabase.h"
//#import "CvViewController.h"
#import "DBInitViewController.h"
#import "AppDelegate.h"
//#import "ToolBoxViewController.h"
//#import "UIView+Origami.h"

@interface TLTableViewController ()

@end

@implementation TLTableViewController

int i=0;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor=PRINCIPAL_TINT_COLOR;
    self.centerView.backgroundColor=PRINCIPAL_TINT_COLOR;
 
    
    NSDictionary *user=[[NSUserDefaults standardUserDefaults] objectForKey:@"GRAD_USER"];
    
    if (user) {
        MINE=[[user objectForKey:@"ID"] intValue];
    }
    
    [self UpdateList];

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)BACK:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
    //[self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [list count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80.0;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"GRADUMATE.sqlite"];
    FMDatabase* db = [FMDatabase databaseWithPath:writableDBPath];
    
    if(![db open])
    {
        NSLog(@"Err %d: %@",[db lastErrorCode],[db lastErrorMessage]);
    }
    
    [db setShouldCacheStatements:YES];
    
    Bubble *TL = [list objectAtIndex:indexPath.row];

    if (TL.IsPublished ==0)
    {
        [db executeUpdate:@"UPDATE GRAD_TIMELINE SET IsPublished=? WHERE ID=?",[NSNumber numberWithInteger:1],[NSNumber numberWithInteger:TL.ID],nil];
    }
    else if (TL.IsPublished ==1)
    {
        [db executeUpdate:@"UPDATE GRAD_TIMELINE SET IsPublished=? WHERE ID=?",[NSNumber numberWithInteger:0],[NSNumber numberWithInteger:TL.ID],nil];
    }
    
    [self UpdateList];
    [table reloadData];

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DashCell *cell;
    static NSString *MagTableIdentifier = @"DashCell";
    cell = (DashCell *)[table dequeueReusableCellWithIdentifier:MagTableIdentifier];
    
     Bubble *TL = [list objectAtIndex:indexPath.row];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DashCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
        int R = indexPath.row % 2;
        if(R==0)
            cell.backgroundColor=[UIColor colorWithRed:0xE1/255.0 green:0xF8/255.0 blue:0xF2/255.0 alpha:1];
        else
            cell.backgroundColor=[UIColor colorWithRed:0xD2/255.0 green:0xF6/255.0 blue:0xEC/255.0 alpha:1];
        
        // Configure the cell...
        cell.TitleLab.text = [[NSString alloc] initWithFormat:@"%@",TL.NAME];
        cell.TitleLab.textColor=[UIColor colorWithRed:0x47/255.f green:0x5C/255.f blue:0x69/255.f alpha:1];
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSInteger year = [calendar component:NSCalendarUnitYear fromDate:TL.bubbleDate];
        NSInteger month = [calendar component:NSCalendarUnitMonth fromDate:TL.bubbleDate];
        NSInteger week = [calendar component:NSCalendarUnitWeekOfMonth fromDate:TL.bubbleDate];
        NSInteger day = [calendar component:NSCalendarUnitDay fromDate:TL.bubbleDate];
        
        NSString *MMM = [self ReturnMounth:month];
    
    NSDate *CurrentDate=[NSDate date];
    NSInteger Currentyear = [calendar component:NSCalendarUnitYear fromDate:CurrentDate];
    NSInteger Currentmonth = [calendar component:NSCalendarUnitMonth fromDate:CurrentDate];
    NSInteger Currentweek = [calendar component:NSCalendarUnitWeekOfMonth fromDate:CurrentDate];
    NSInteger Currentday = [calendar component:NSCalendarUnitDay fromDate:CurrentDate];
    
    if(year>Currentyear)
        cell.DateLab.text = [[NSString alloc] initWithFormat:@"Will start in %@ %ld",MMM, (long)year];
    else if(year<Currentyear)
       cell.DateLab.text = [[NSString alloc] initWithFormat:@"Started in %@ %ld",MMM, (long)year];
    else if(month<Currentmonth)
       cell.DateLab.text = [[NSString alloc] initWithFormat:@"Started in %@ %ld",MMM, (long)year];
    else if(month>Currentmonth)
        cell.DateLab.text = [[NSString alloc] initWithFormat:@"Will start in %@ %ld",MMM, (long)year];
       else if(day<Currentday)
        cell.DateLab.text = [[NSString alloc] initWithFormat:@"Started in %@ %ld",MMM, (long)year];
    else  if(day>Currentday)
        cell.DateLab.text = [[NSString alloc] initWithFormat:@"Will start in %@ %ld",MMM, (long)year];
    else
        cell.DateLab.text = [[NSString alloc] initWithFormat:@"Starts today"];
        
    
    
        
        if (TL.IsPublished==1) {
            [cell.CheckBtn setBackgroundImage:[UIImage imageNamed:@"ListChecked.png"] forState:UIControlStateNormal];
        }
        else if (TL.IsPublished==0)
        {
            [cell.CheckBtn setBackgroundImage:[UIImage imageNamed:@"ListUnchecked.png"] forState:UIControlStateNormal];
        }
    
    
    return cell;
}

- (NSString *)ReturnMounth: (NSInteger)MOUNTH_F
{
    NSString *MOUNTH;
    switch (MOUNTH_F)
    {
        case 1:
            MOUNTH = @"JAN";
            break;
        case 2:
            MOUNTH = @"FEB";
            break;
        case 3:
            MOUNTH = @"MAR";
            break;
        case 4:
            MOUNTH = @"APR";
            break;
        case 5:
            MOUNTH = @"MAY";
            break;
        case 6:
            MOUNTH = @"JUN";
            break;
        case 7:
            MOUNTH = @"JUL";
            break;
        case 8:
            MOUNTH = @"AUG";
            break;
        case 9:
            MOUNTH = @"SEPT";
            break;
        case 10:
            MOUNTH = @"OCT";
            break;
        case 11:
            MOUNTH = @"NOV";
            break;
        case 12:
            MOUNTH = @"DEC";
            break;
            
        default:
            break;
    }
    
    return MOUNTH;
}

-(void)UpdateList
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"GRADUMATE.sqlite"];
    FMDatabase* db = [FMDatabase databaseWithPath:writableDBPath];
    
    if(![db open])
    {
        NSLog(@"Err %d: %@",[db lastErrorCode],[db lastErrorMessage]);
    }
    
    [db setShouldCacheStatements:YES];
    
    NSString*  sql;
    sql = [[NSString alloc] initWithFormat:@"SELECT* FROM GRAD_TIMELINE WHERE IsAccepted=1 ORDER BY date(AchDATE)"];
    
    FMResultSet*    rs = [db executeQuery:sql];
    
    list = [[NSMutableArray alloc] init];
    Bubble *TimeLists;
    
    while([rs next])
    {
        TimeLists = [Bubble tinelineForColumn:rs];
        
        [list addObject:TimeLists];
        TimeLists=nil;
    }
    
    [db close];
}

@end

@implementation DashCell


//- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
//{
//    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
//    if (self) {
//        // Initialization code
//        
//        
//    }
//    return self;
//}
//
//- (void)setSelected:(BOOL)selected animated:(BOOL)animated
//{
//    [super setSelected:selected animated:animated];
//
//}

@end



//
//  SeeMoreViewController.h
//  GRADUMATE
//
//  Created by Malek Mansour on 6/9/15.
//  Copyright (c) 2015 SlickStone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Bubble.h"
#import "FMDatabase.h"
#import "COMMENT.h"
#import "JSONHelper.h"
//#import "ChatViewController.h"
//#import "LifeLineViewController.h"


@interface SeeMoreViewController : UIViewController <UITextFieldDelegate>
{
    IBOutlet  UIButton *actionButton;
    
    
    int REF_NUMBER;
    
    IBOutlet UILabel *NAMEL;
    IBOutlet UILabel *DESCL;
    IBOutlet UILabel *DATEL;
    IBOutlet UILabel *CONGLABEL;
    IBOutlet UILabel *UniversityLab;
    IBOutlet UILabel *BubbleLab;
    IBOutlet UILabel *UniversityWebsite;
    IBOutlet UITextView*descTextView;
    IBOutlet UIView*usersV;
    IBOutlet UITableView*table;
    IBOutlet UITextField*bubbleTitle;
    IBOutlet UITextField*bubbleDate;
    IBOutlet UIView*bubblesView;
    IBOutlet UITableView*bubblesTable;
    IBOutlet UIView*editDescView;
    IBOutlet UIDatePicker *Datepicker;

    
    
    IBOutlet UIDatePicker *DATE;
    
    IBOutlet UIButton *Update;
    IBOutlet UIButton *Edit;

    IBOutlet UISwitch *IsCompleted;
    IBOutlet UISwitch *IsPublished;
    IBOutlet UISwitch *ONTL;

    IBOutlet UIView *CongView ;
    IBOutlet UIView *CongView2;

    IBOutlet UIScrollView *SCROLL;
    
    
    IBOutlet UIImageView *AchievementImg;
    IBOutlet UIImageView *UniversityImg;

    int ReloadTimeline;
}

@property(nonatomic, readwrite) Bubble *TimeObject;
@property(nonatomic, readwrite) NSInteger COMMENT;
@property(nonatomic, retain) NSArray* AllBubbles;

//@property (strong, nonatomic) UIWindow *window;
@end

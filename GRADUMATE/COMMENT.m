//
//  COMMENT.m
//  GRADUMATE
//
//  Created by Malek Mansour on 5/19/15.
//  Copyright (c) 2015 SlickStone. All rights reserved.
//

#import "COMMENT.h"

@implementation COMMENT


+(instancetype)commentFromDic:(NSDictionary*)dic{
    COMMENT *comment=[[COMMENT alloc] init];
    comment.TEXT= [dic objectForKey:@"COMMENT"];
    comment.ID_USERRECEIVER= [dic objectForKey:@"ID"];
    comment.userImage= [dic objectForKey:@"IMAGE"];
    comment.userName= [dic objectForKey:@"NAME"];
    return comment;

}


+(instancetype)newCommentwithText:(NSString*)Text{
   COMMENT *comment=[[COMMENT alloc] init];
    
    NSDictionary *user=[[NSUserDefaults standardUserDefaults] objectForKey:@"GRAD_USER"];
    NSString* userid=[user objectForKey:@"LOGIN"] ;
     comment.userImage=[NSString stringWithFormat:@"%@%@.png",@IMAGE_BASE_URL,userid];
    
    
    comment.TEXT=Text;
    comment.ID_USERRECEIVER=[user objectForKey:@"ID"];
    return comment;
    
}

@end

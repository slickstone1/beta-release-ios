//
//  TIMELine.m
//  GRADUMATE
//
//  Created by Malek Mansour on 3/15/15.
//  Copyright (c) 2015 SlickStone. All rights reserved.
//

#import "Bubble.h"
#import "FMDatabase.h"
@implementation Bubble




+(instancetype)tinelineForColumn:(FMResultSet*)rs{
  Bubble*  TimeLists = [[Bubble alloc] init];
      TimeLists.ID = [NSNumber numberWithInt:[rs intForColumn:@"ID"]];
      TimeLists.NAME = [rs stringForColumn:@"NAME"];
      TimeLists.KIND = [rs stringForColumn:@"KIND"];
      TimeLists.DESC = [rs stringForColumn:@"DESC"];
      TimeLists.bubbleDate = [rs dateForColumn:@"AchDATE"];
      TimeLists.image = [rs stringForColumn:@"LINK"];
      TimeLists.PID = [NSNumber numberWithInt:[rs intForColumn:@"PID"]];
    
    
    
//    TimeLists.STARTDATE = [rs dateForColumn:@"STARTDATE"];
    TimeLists.IsCompleted = [rs intForColumn:@"IsCompleted"];
    TimeLists.LINK = [rs stringForColumn:@"LINK"];
    TimeLists.IsHidden = [rs intForColumn:@"IsHidden"];
    TimeLists.IsPublished = [rs intForColumn:@"IsPublished"];
    TimeLists.NBCOMMENT = [rs intForColumn:@"NBCOMMENT"];
    TimeLists.TLID = [rs intForColumn:@"TLID"];
    
    
    return TimeLists;

}

+(instancetype)bubbleFromDic:(NSDictionary*)dic{
  Bubble*  Time = [[Bubble alloc] init];
    
    Time.ID = [dic valueForKey:@"IDCOURSE"] ;
    Time.NAME = [dic valueForKey:@"NAME"];
    Time.KIND = [dic valueForKey:@"KIND"];
    Time.DESC = [dic valueForKey:@"DESC"];
    Time.image= [dic valueForKey:@"location"];
    Time.PID= [dic valueForKey:@"IDPARENT"];
    //  Time.NBCOMMENT = [[[LISTS objectAtIndex:iii] valueForKey:@"NB"] integerValue];
    //  Time.TLID = [[[LISTS objectAtIndex:iii] valueForKey:@"TLID"] integerValue];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss.S"];
    
    NSString *dateString=[dic valueForKey:@"DATE"];
    Time.bubbleDate =[dateFormat dateFromString:dateString];
//    Time.bubbleDate = [self parseDate:[dic valueForKey:@"DATE"] format:@"yyyy-MM-dd 00:00:00.0"];
//    NSLog(@"DATE: %@",[dic valueForKey:@"DATE"]);
//    ;
    
        return Time;

}
+ (NSDate*)parseDate:(NSString*)inStrDate format:(NSString*)inFormat {
    NSDateFormatter* dtFormatter = [[NSDateFormatter alloc] init];
//    [dtFormatter setLocale:[NSLocale systemLocale]];
    [dtFormatter setDateFormat:inFormat];
    NSDate* dateOutput = [dtFormatter dateFromString:inStrDate];
    return dateOutput;
}

-(void)updateWithDate:(NSDate*)date{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"GRADUMATE.sqlite"];
    FMDatabase* db = [FMDatabase databaseWithPath:writableDBPath];
    if(![db open])
    {
        NSLog(@"Err %d: %@",[db lastErrorCode],[db lastErrorMessage]);
    }
    [db executeUpdate:@"UPDATE GRAD_TIMELINE SET AchDate=? WHERE ID=?",date,[NSNumber numberWithInteger:self.ID],nil];
    
    
}
- (void)encodeWithCoder:(NSCoder *)encoder {
    
    
//    [encoder encodeObject:_ID forKey:@"ID"];
    [encoder encodeObject:_NAME forKey:@"NAME"];
    [encoder encodeObject:_KIND forKey:@"KIND"];
    [encoder encodeObject:_DESC forKey:@"DESC"];
    [encoder encodeObject:_bubbleDate forKey:@"AchDATE"];
//    [encoder encodeObject:_image forKey:@"LINK"];
//    [encoder encodeObject:_PID forKey:@"PID"];
//    [encoder encodeObject:[NSNumber numberWithInteger:_IsCompleted] forKey:@"IsCompleted"];
//    [encoder encodeObject:_LINK forKey:@"LINK"];
//    [encoder encodeObject:[NSNumber numberWithInteger:_IsHidden] forKey:@"IsHidden"];
//    [encoder encodeObject:[NSNumber numberWithInteger:_IsPublished] forKey:@"IsPublished"];
//    [encoder encodeObject:[NSNumber numberWithInteger:_NBCOMMENT] forKey:@"NBCOMMENT"];
//    [encoder encodeObject:[NSNumber numberWithInteger:_TLID] forKey:@"TLID"];

}

- (instancetype)initWithCoder:(NSCoder *)decoder {
//    self = [super initWithCoder:decoder];
//    if (self) {
        _NAME = [decoder decodeObjectForKey:@"NAME"];
        _KIND = [decoder decodeObjectForKey:@"KIND"];
        _DESC = [decoder decodeObjectForKey:@"DESC"];
        _bubbleDate = [decoder decodeObjectForKey:@"AchDATE"];
//    }
    return self;
}





@end

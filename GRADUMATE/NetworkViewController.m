//
//  NetworkViewController.m
//  GRADUMATE
//
//  Created by Ahmad Jarray on 10/12/2015.
//  Copyright © 2015 Ahmad Jarray. All rights reserved.
//

#import "NetworkViewController.h"

#import "UserCell.h"
#import "User.h"
#import "JSONHelper.h"
#import "TimelineViewController.h"

@interface NetworkViewController (){

    NSMutableArray *users;
}

@end

@implementation NetworkViewController
- (void) viewDidLoad
{
    
    users=[NSMutableArray array];
//    [JSONHelper getUsersDelegate:self tag:0];
    int user=[[[[NSUserDefaults standardUserDefaults] objectForKey:@"GRAD_USER"]objectForKey:@"ID"] integerValue];

    [JSONHelper GetListUsersWithId:[NSNumber numberWithInt:user] delegate:self tag:0];

}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section==0) {
        if ([users count]>4)
            return 4;
    }
   
    return [users count];

}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UserCell* cell =[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
     id user= [users objectAtIndex:indexPath.row];
    [cell configWithUser:user];
    
    return cell;

}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    [self performSegueWithIdentifier:@"TimeLine" sender:[users objectAtIndex:indexPath.row]];
}


- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
   return  CGSizeMake((float)SCREENWIDTH /4.0, SCREENWIDTH /4.0 +40);

}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        reusableview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
    }
    
    return reusableview;
}


- (IBAction) ShowSideMenu:(id)sender
{
    [[Singleton sharedInstance].drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
    
}


-(void)receiveData:(id)responseData tag:(int)tag{
   
        users=responseData;
        
        [collection  reloadData];

}


#pragma mark - prepareForSegue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"TimeLine"]) {
        ((TimelineViewController*)segue.destinationViewController).user= sender;
        
    }else{
    }
}



@end

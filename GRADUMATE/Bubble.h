//
//  TIMELine.h
//  GRADUMATE
//
//  Created by Malek Mansour on 3/15/15.
//  Copyright (c) 2015 SlickStone. All rights reserved.
//

#import <Foundation/Foundation.h>
@class FMResultSet;
@interface Bubble : NSObject

@property(nonatomic,readwrite) NSNumber* ID;
@property(nonatomic,readwrite) NSNumber* PID;
@property(nonatomic,readwrite) NSInteger ONTL;
@property(nonatomic,readwrite) NSInteger IsCompleted;
@property(nonatomic,readwrite) NSInteger IsPublished;
@property(nonatomic,readwrite) NSInteger IsHidden;
@property(nonatomic,readwrite) NSInteger NBCOMMENT;
@property(nonatomic,readwrite) NSInteger TLID;


@property(nonatomic,strong) NSString *NAME;
@property(nonatomic,strong) NSString *DESC;
@property(nonatomic,strong) NSString* image;
@property(nonatomic,strong) NSString *TYPE;


@property(nonatomic,strong) NSDate *bubbleDate;
//@property(nonatomic,strong) NSDate *STARTDATE;

@property(nonatomic,strong) NSString *KIND;
@property(nonatomic,strong) NSString *LINK;

+(instancetype)tinelineForColumn:(FMResultSet*)rs;
+(instancetype)bubbleFromDic:(NSDictionary*)dic;
@end

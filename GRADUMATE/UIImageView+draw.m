//
//  UIImageView+draw.m
//  GRADUMATE
//
//  Created by Ahmad Jarray on 01/12/2015.
//  Copyright © 2015 Ahmad Jarray. All rights reserved.
//

#import "UIImageView+draw.h"

@implementation UIImageView (draw)

-(void)drawRectWithdegree:(float)degree
{
    
    CAShapeLayer *circle=[CAShapeLayer layer];
    circle.path=[UIBezierPath bezierPathWithArcCenter:CGPointMake(self.frame.size.width/2, self.frame.size.width/2) radius:self.frame.size.width/2+5 startAngle:2*M_PI*0-M_PI_2 endAngle:M_PI-(2*M_PI*1-M_PI_2)*degree clockwise:NO].CGPath;
    circle.fillColor=[UIColor clearColor].CGColor;
    
//    switch (self.tag)
//    {
//        case 1:
//            circle.strokeColor=[UIColor colorWithRed:0x91/255.0 green:0xE2/255.0 blue:0x31/255.0 alpha:1].CGColor;
//            break;
//        case 2:
//            circle.strokeColor=[UIColor colorWithRed:0x4F/255.0 green:0x00/255.0 blue:0xEB/255.0 alpha:1].CGColor;
//            break;
//        case 3:
//            circle.strokeColor=[UIColor colorWithRed:0xF4/255.0 green:0x00/255.0 blue:0x29/255.0 alpha:1].CGColor;
//            break;
//        case 4:
//            circle.strokeColor=[UIColor colorWithRed:0x42/255.0 green:0x00/255.0 blue:0xBE/255.0 alpha:1].CGColor;
//            break;
//        case 5:
//            circle.strokeColor=[UIColor colorWithRed:0xF1/255.0 green:0x56/255.0 blue:0x0C/255.0 alpha:1].CGColor;
//            break;
//        case 6:
//            circle.strokeColor=[UIColor colorWithRed:0xE2/255.0 green:0x34/255.0 blue:0x31/255.0 alpha:1].CGColor;
//            break;
//        case 7:
//            circle.strokeColor=[UIColor colorWithRed:0x63/255.0 green:0x79/255.0 blue:0x75/255.0 alpha:1].CGColor;
//            break;
//        case 8:
//            circle.strokeColor=[UIColor colorWithRed:0x33/255.0 green:0x99/255.0 blue:0xCC/255.0 alpha:1].CGColor;
//            break;
//    }
    circle.strokeColor=[PRINCIPAL_BORDER_COLOR CGColor];
    circle.lineWidth=4;
    
    
    CABasicAnimation *animation=[CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    animation.duration=2;
    animation.removedOnCompletion=NO;
    animation.fromValue=@(0);
    animation.toValue=@(1);
    animation.timingFunction=[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    [circle addAnimation:animation forKey:@"drawCircleAnimation"];
    
    [self.layer.sublayers makeObjectsPerformSelector:@selector(removeFromSuperlayer)];
    [self.layer addSublayer:circle];
}

@end

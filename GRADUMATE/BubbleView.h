//
//  BubbleView.h
//  GRADUMATE
//
//  Created by hfetoui on 24/12/2015.
//  Copyright © 2015 Ahmad Jarray. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BubbleView : UIView


@property(nonatomic,retain)IBOutlet UIImageView*bubbleImage;

@end

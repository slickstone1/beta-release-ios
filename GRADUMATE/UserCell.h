//
//  UserCell.h
//  GRADUMATE
//
//  Created by Ahmad Jarray on 10/12/2015.
//  Copyright © 2015 Ahmad Jarray. All rights reserved.
//

@class User;
@interface UserCell : UICollectionViewCell{
    IBOutlet UILabel *labelName;
    IBOutlet UIImageView*image;

}
-(void)configWithUser:(User*)user;
@end

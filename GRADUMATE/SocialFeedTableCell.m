//
//  SimpleTableCell.m
//  SimpleTable
//
//  Created by Simon Ng on 28/4/12.
//  Copyright (c) 2012 Appcoda. All rights reserved.
//

#import "SocialFeedTableCell.h"
#import "SOCIALFEED.h"
#import "JSONHelper.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation SocialFeedTableCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configWithFeed:(SOCIALFEED*)feed{
    
    NSString *kind = feed.KIND;
    
    UIImage *mimage ;
    if ([kind isEqualToString:@"CERTIFICATE"]) {
        mimage=[UIImage imageNamed:@"TLcertificate"];
    }
    else if ([kind isEqualToString:@"ACTIVITY"]) {
        mimage=[UIImage imageNamed:@"TLactivity"];
    }
    else if ([kind isEqualToString:@"COURSE"]) {
        mimage=[UIImage imageNamed:@"TLcourse"];
    }
    else if ([kind isEqualToString:@"VIDEO"]) {
        mimage=[UIImage imageNamed:@"TLvideo"];
    }
    else if ([kind isEqualToString:@"GOAL"]) {
        mimage=[UIImage imageNamed:@"TLgoal"];
    }
    else if ([kind isEqualToString:@"OBJECTIVE"]) {
        mimage=[UIImage imageNamed:@"TLobjective"];
    }
    else if ([kind isEqualToString:@"EVENT"]) {
        mimage=[UIImage imageNamed:@"TLevent"];
    }
    else if ([kind isEqualToString:@"ANNOUNCEMENT"]) {
        mimage=[UIImage imageNamed:@"TLannouncement"];
    }
    
    KindImageView.image=mimage;
    
    thumbnailImageView.layer.cornerRadius= thumbnailImageView.frame.size.width/2;
    [thumbnailImageView setClipsToBounds:YES];
    
    thumbnailImageView.layer.borderWidth=2.5;
    thumbnailImageView.layer.borderColor=PRINCIPAL_BORDER_COLOR.CGColor;
    
    [thumbnailImageView sd_setImageWithURL:[NSURL URLWithString:feed.userImage] placeholderImage:[UIImage imageNamed:@"58"]];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    long year = [calendar component:NSCalendarUnitYear fromDate:feed.DATE];
    long month = [calendar component:NSCalendarUnitMonth fromDate:feed.DATE];
    long hour = [calendar component:NSCalendarUnitHour fromDate:feed.DATE];
    long minutes = [calendar component:NSCalendarUnitMinute fromDate:feed.DATE];
    long day = [calendar component:NSCalendarUnitDay fromDate:feed.DATE];
    
    bubbleLab.text=feed.NAME_ITEM;
    
    usernameLab.text=feed.NAME;
    //       actionLab.text=[[NSString alloc] initWithFormat:@"get a certification for  the %@",feed.KIND];
    contentLab.text=feed.TEXT;
            
    dateLab.text=[[NSString alloc] initWithFormat:@"%lu-%lu-%lu at %lu:%lu",year,month,day,hour,minutes];
   
    NSString *MyUserID=[[[NSUserDefaults standardUserDefaults] objectForKey:@"GRAD_USER"] objectForKey:@"ID"];
   
    self.rightButtons = @[[MGSwipeButton buttonWithTitle:@"Clone" backgroundColor:PRINCIPAL_TINT_COLOR callback:
                           ^BOOL(MGSwipeTableCell * sender)
                           {
                               
                               if (feed.ID_USERSENDER== [MyUserID integerValue]) {
                                   [[[UIAlertView alloc]initWithTitle:@"Attention!" message:@"You can't clone your own bubble." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                               }else{
                                   NSLog(@"Convenience callback received (right).");
                                   [JSONHelper CloneFeedWithId:feed.ID_ITEM ForUserWithId:feed.ID_USERSENDER delegate:self tag:0];
                               }
                               return NO;
                           }
                           ]];
    
//    self.rightButtons = @[[MGSwipeButton buttonWithTitle:@"Clone" backgroundColor:PRINCIPAL_TINT_COLOR]];
    
    self.rightSwipeSettings.transition = MGSwipeTransitionBorder;

}

-(void)receiveData:(id)responseData tag:(int)tag{
    
    NSLog(@"%@",responseData);
    if ([[responseData objectForKey:@"ETAT"] isEqualToString:@"YES"]) {
         [[[UIAlertView alloc] initWithTitle:nil message:@"Clone succeed" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }else
         [[[UIAlertView alloc] initWithTitle:nil message:@"Would you please repeat" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
}

@end

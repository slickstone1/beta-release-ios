//
//  StartViewController.m
//  GRADUMATE
//
//  Created by Ahmad Jarray on 17/11/2015.
//  Copyright © 2015 Ahmad Jarray. All rights reserved.
//

#import "StartViewController.h"
#import <SDWebImage/SDImageCache.h>

@interface StartViewController ()

@end

@implementation StartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    NSDictionary *user=[[NSUserDefaults standardUserDefaults] objectForKey:@"GRAD_USER"];
    
   
    if (user) {
        // NSLog(user);
        [[SDImageCache sharedImageCache] removeImageForKey:[NSString stringWithFormat:@"%@%@.png",@IMAGE_BASE_URL,[user objectForKey:@"LOGIN"]]];
        MMDrawerController *drawerController=[self.storyboard instantiateViewControllerWithIdentifier:@"main"];
        [drawerController setShowsShadow:YES];
        
        [drawerController setMaximumRightDrawerWidth:[[UIScreen mainScreen] bounds].size.width-50];
        [drawerController setMaximumLeftDrawerWidth:[[UIScreen mainScreen] bounds].size.width-50];
//        [drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
        [drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
        
        self.viewControllers=@[drawerController];

        [Singleton sharedInstance].drawerController=drawerController;
    } else{
        
        self.viewControllers=@[[self.storyboard instantiateViewControllerWithIdentifier:@"LOGON"]];
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

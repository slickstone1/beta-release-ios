//
//  COMMENT.h
//  GRADUMATE
//
//  Created by Malek Mansour on 5/19/15.
//  Copyright (c) 2015 SlickStone. All rights reserved.
//

@interface COMMENT : NSObject


@property(nonatomic, readwrite) NSInteger ID;
@property(nonatomic, readwrite) NSInteger TYPE;
@property(nonatomic, readwrite) NSString *TEXT;
@property(nonatomic, readwrite) NSString *NAME;
@property(nonatomic, readwrite) NSInteger ID_ITEM;
@property(nonatomic, readwrite) NSNumber* ID_USERRECEIVER;
@property(nonatomic, readwrite) NSInteger ID_USERSENDER;
@property(nonatomic, readwrite) NSString* userImage;
@property(nonatomic, readwrite) NSString* userName;
@property(nonatomic, readwrite) NSDate  *DATE;

+(instancetype)commentFromDic:(NSDictionary*)dic;
+(instancetype)newCommentwithText:(NSString*)Text;
@end

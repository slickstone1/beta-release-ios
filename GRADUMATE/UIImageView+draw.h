//
//  UIImageView+draw.h
//  GRADUMATE
//
//  Created by Ahmad Jarray on 01/12/2015.
//  Copyright © 2015 Ahmad Jarray. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (draw)
-(void)drawRectWithdegree:(float)degree;
@end

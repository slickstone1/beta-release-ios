//
//  SocialFeedViewController.h
//  GRADUMATE
//
//  Created by Malek Mansour on 7/4/15.
//  Copyright (c) 2015 SlickStone. All rights reserved.
//


@class Bubble;
@interface SocialFeedViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UINavigationControllerDelegate>
{

    IBOutlet UITableView *table;
   
}

@property (strong, nonatomic) Bubble *timeline;


@end

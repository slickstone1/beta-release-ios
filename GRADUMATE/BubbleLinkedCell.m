//
//  BubbleLinkedCell.m
//  GRADUMATE
//
//  Created by hfetoui on 24/12/2015.
//  Copyright © 2015 Ahmad Jarray. All rights reserved.
//

#import "BubbleLinkedCell.h"
#import <FontAwesome/NSString+FontAwesome.h>
#import "Bubble.h"

@implementation BubbleLinkedCell

- (void)awakeFromNib {
    // Initialization code
}

-(void)initCell{
    _checked.text=[NSString fontAwesomeIconStringForIconIdentifier:@"fa-check"];
    _checked.layer.cornerRadius=_checked.frame.size.width/2;
    _checked.layer.masksToBounds=YES;
    
    _cellView.layer.cornerRadius=5;
    _cellView.layer.masksToBounds=YES;
     self.selectionStyle=UITableViewCellSelectionStyleNone;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

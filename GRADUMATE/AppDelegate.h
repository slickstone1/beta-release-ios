//
//  AppDelegate.h
//  GRADUMATE
//
//  Created by Ahmad Jarray on 17/11/2015.
//  Copyright © 2015 Ahmad Jarray. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (readwrite, nonatomic) NSInteger NOTIFICATION;
@property (readwrite, nonatomic) NSInteger Comment_Notification;

@end


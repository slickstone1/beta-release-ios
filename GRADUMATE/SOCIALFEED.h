//
//  SOCIALFEED.h
//  GRADUMATE
//
//  Created by Malek Mansour on 8/13/15.
//  Copyright © 2015 SlickStone. All rights reserved.
//


@interface SOCIALFEED : NSObject




@property(nonatomic, readwrite) NSInteger ID;
@property(nonatomic, readwrite) NSInteger TYPE;
@property(nonatomic, readwrite) NSString *TEXT;
@property(nonatomic, readwrite) NSString *KIND;
@property(nonatomic, readwrite) NSString *NAME;
@property(nonatomic, readwrite) NSString *NAME_ITEM;
@property(nonatomic, readwrite) NSInteger ID_USERRECEIVER;
@property(nonatomic, readwrite) NSInteger ID_ITEM;
@property(nonatomic, readwrite) NSInteger ID_USERSENDER;
@property(nonatomic, readwrite) NSDate  *DATE;
@property(nonatomic, readwrite) NSString  *userImage;


+(instancetype)feedFromDic:(NSDictionary*)dic;
@end

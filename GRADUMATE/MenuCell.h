//
//  MenuCell.h
//  GRADUMATE
//
//  Created by Ahmad Jarray on 11/12/2015.
//  Copyright © 2015 Ahmad Jarray. All rights reserved.
//

@interface MenuCell : UITableViewCell{
    IBOutlet UILabel *labelName;
    IBOutlet UIImageView*imageview;
    IBOutlet UILabel *labelNotification;
    
}
-(void)configWithImage:(UIImage*)aimage title:(NSString*)title notification:(NSString*)n;
@end

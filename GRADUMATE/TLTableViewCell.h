//
//  TLTableViewCell.h
//  GRADUMATE
//
//  Created by Ahmad Jarray on 19/11/2015.
//  Copyright © 2015 Ahmad Jarray. All rights reserved.
//

#import "Bubble.h"

@interface TLTableViewCell : UITableViewCell{
    IBOutlet UIImageView *imageview;
    IBOutlet UILabel *lABELDate,*lableName,*labelCom;
    IBOutlet UIView *todayView,*timeLineView;
    UIColor *origninalcolor;
}

@property (nonatomic,retain)Bubble*bubble;
-(void)configwiThTimeLine:(Bubble*)timeLine future:(BOOL)future;

@end

//
//  JSONHelper.h
//  
//
//  Created by Malek Mansour on 08/06/14.
//  Copyright (c) 2014 Lekma. All rights reserved.

//

#import "JSONHelper.h"
#include <CommonCrypto/CommonCryptor.h>
#import <AFNetworking/AFHTTPRequestOperationManager.h>
#import "Bubble.h"

@implementation JSONHelper

+(void)parseJsonResponse:(NSString *)urlString withDelegate:(id<helperDelegate>)delegate
{
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
        
        if ([data length] > 0 && connectionError == nil){
            NSString *string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSLog(@"received data : %@",string);
            NSDictionary *JSON =  [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &connectionError];
            [delegate receiveData:JSON];
        }
//        else{
//            [[[UIAlertView  alloc]initWithTitle:@"Failure!" message:@"The Internet connection appears to be offline." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
//        }
    }];
}

+(void)getDataPostFrom:(NSString *)urlString postData:(NSDictionary*)postdic delegate:(id<helperDelegate>)delegate tag:(int)tag
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager POST:[NSString stringWithFormat:@"%@%@",@BASE_URL,urlString] parameters:postdic success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"JSON: %@", responseObject);
         NSError *error;
         NSDictionary *JSON=[NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
         
         [delegate receiveData:JSON tag:tag];

     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         
         [delegate hideHud];
         NSLog(@"Error: %@", error);
         [[[UIAlertView alloc] initWithTitle:nil message:@"Would you please repeat" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
     }];
}

+(void)getTimeLineforUserId:(NSNumber*)userId delegate:(id<helperDelegate>)delegate tag:(int)tag
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:[NSString stringWithFormat:@"%@ListBubblePublic/%@",@BASE_URL,userId] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
          NSError *error;
         id JSON =  [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
         [delegate receiveData:JSON tag:tag];
         
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
         [[[UIAlertView alloc] initWithTitle:nil message:@"Would you please repeat" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
     }];
}

+(void)saveBubble:(Bubble*)bubble withImage:(UIImage*)image delegate:(id<helperDelegate>)delegate tag:(int)tag
{

    NSString *MyUserID=[[[NSUserDefaults standardUserDefaults] objectForKey:@"GRAD_USER"] objectForKey:@"ID"];

    __block long CreatedBubbleID;
    NSString *url = [[NSString alloc] initWithFormat:@"%@AddObject/%@/%@/%@/%d/%d",@BASE_URL,bubble.NAME,bubble.KIND,MyUserID,1,true];
    url =[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    
    NSString *ImgString=[UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    NSDictionary *params = @{@"ICON":ImgString, @"DATE":bubble.bubbleDate, @"DESC":bubble.DESC};
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSError *error;
         NSDictionary *JSONResponse = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: &error];
         if (JSONResponse)
         {
                  [delegate receiveData:JSONResponse tag:tag];

             [[[UIAlertView alloc] initWithTitle:@"New bubble" message:@"new bubble succuessfully created!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
         }
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error creating bubble: %@", error);
         [[[UIAlertView alloc] initWithTitle:@"New bubble" message:@"An error occured while creating new Bubble!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
     }];
}

+(void)getMyTimeLineDelegate:(id<helperDelegate>)delegate tag:(int)tag{
   
    
    float lastUpdate;
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"lastUpdate"]) {
        lastUpdate= [[[NSUserDefaults standardUserDefaults] objectForKey:@"lastUpdate"] timeIntervalSince1970];
    }else
        lastUpdate=0;
    
    
    NSString *url = [[NSString alloc] initWithFormat:@"%@ListCourses/%@",@BASE_URL,[[[NSUserDefaults standardUserDefaults] objectForKey:@"GRAD_USER"] objectForKey:@"ID"]];
    NSString*str=[NSString stringWithFormat:@"%.f",lastUpdate];
    NSDictionary *params = @ {@"DATE" :str};

     AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];

    [manager POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"lastUpdate"];
         
         NSError *error;
         id JSON =  [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
         [delegate receiveData:JSON tag:tag];
         
     }
         failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
         [[[UIAlertView alloc] initWithTitle:nil message:@"Would you please repeat" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
     }];
}

+(void)GetListUsersWithId:(NSNumber*)userID delegate:(id<helperDelegate>)delegate tag:(int)tag{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    float lastUpdate;
    //    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"lastUpdate"]) {
    //        lastUpdate= [[[NSUserDefaults standardUserDefaults] objectForKey:@"lastUpdate"] timeIntervalSince1970];
    //    }else
    lastUpdate=0;
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:[NSString stringWithFormat:@"%@ListUsers/%d",@BASE_URL,(int)userID] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         id JSON =  [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
         [delegate receiveData:JSON tag:tag];
         
     }
         failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
         [[[UIAlertView alloc] initWithTitle:nil message:@"Would you please repeat" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
     }];
}

+(void)ShareBubble:(Bubble*)bubble title:(NSString*)title withUsers:(NSString*)users delegate:(id<helperDelegate>)delegate tag:(int)tag{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    float lastUpdate;
    //    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"lastUpdate"]) {
    //        lastUpdate= [[[NSUserDefaults standardUserDefaults] objectForKey:@"lastUpdate"] timeIntervalSince1970];
    //    }else
    lastUpdate=0;
    
    NSString *MyUserID=[[[NSUserDefaults standardUserDefaults] objectForKey:@"GRAD_USER"] objectForKey:@"ID"];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSString*URLString=[NSString stringWithFormat:@"%@Share/%@/%@/%@/%@/%@",@BASE_URL,bubble.ID,MyUserID,title,[NSDate date],users];
    URLString=[URLString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [manager GET:URLString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         id JSON =  [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
         [delegate receiveData:JSON tag:tag];
         
     }
         failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
         [[[UIAlertView alloc] initWithTitle:nil message:@"Would you please repeat" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
     }];
    
}
+(void)CloneFeedWithId:(NSInteger*)bubbleID ForUserWithId:(NSInteger*)userId delegate:(id<helperDelegate>)delegate tag:(int)tag{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    float lastUpdate;
    //    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"lastUpdate"]) {
    //        lastUpdate= [[[NSUserDefaults standardUserDefaults] objectForKey:@"lastUpdate"] timeIntervalSince1970];
    //    }else
    lastUpdate=0;
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:[NSString stringWithFormat:@"%@Clone/%d/%d",@BASE_URL,(int)bubbleID,(int)userId] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         id JSON =  [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
         [delegate receiveData:JSON tag:tag];
         
     }
         failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
         [[[UIAlertView alloc] initWithTitle:nil message:@"Would you please repeat" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
     }];
}

+(void)getFeedForUserId:(int)userId inPage:(int)page delegate:(id<helperDelegate>)delegate tag:(int)tag
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:[NSString stringWithFormat:@"%@SocialFeedWS/%d/%d",@BASE_URL,page,userId] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSError *error;
         id JSON =  [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
         [delegate receiveData:JSON tag:tag];
         
     }
         failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
         [[[UIAlertView alloc] initWithTitle:nil message:@"Would you please repeat" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
     }];
}


+(void)getCommentsForShareId:(int)shareId inPage:(int)page delegate:(id<helperDelegate>)delegate tag:(int)tag
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:[NSString stringWithFormat:@"%@CommentShare/%d",@BASE_URL,shareId] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSError *error;
         id JSON =  [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
         [delegate receiveData:JSON tag:tag];
         
     }
         failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
         [[[UIAlertView alloc] initWithTitle:nil message:@"Would you please repeat" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
     }];
}

+(void)addComments:(NSString*)comment ForShareId:(int)shareId atIndex:(int)page delegate:(id<helperDelegate>)delegate tag:(int)tag
{
//    AddComment/{IDSOCIAL}/{IDUSER}/{COMMENT}
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    int userid=[[[[NSUserDefaults standardUserDefaults] objectForKey:@"GRAD_USER"] objectForKey:@"ID"] intValue];
    NSDictionary* postdic=@{@"COMMENT":comment};
    [manager POST:[NSString stringWithFormat:@"%@AddComment/%d/%d/%d",@BASE_URL,shareId,userid,page] parameters:postdic success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSError *error;
         id JSON =  [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
         [delegate receiveData:JSON tag:tag];
         
     }
         failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
         [[[UIAlertView alloc] initWithTitle:nil message:@"Would you please repeat" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
     }];
}

+(void)getUsersDelegate:(id<helperDelegate>)delegate tag:(int)tag
{
    //    AddComment/{IDSOCIAL}/{IDUSER}/{COMMENT}
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    int userid=[[[[NSUserDefaults standardUserDefaults] objectForKey:@"GRAD_USER"] objectForKey:@"ID"] intValue];
    NSString *url =[NSString stringWithFormat:@"%@ListFriends/%d",@BASE_URL,userid];
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSError *error;
         id JSON =  [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
         [delegate receiveData:JSON tag:tag];
         
     }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
          [[[UIAlertView alloc] initWithTitle:nil message:@"Would you please repeat" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
     }];
}

+(NSString*)urlEscapeString:(NSString *)unencodedString
{
    CFStringRef originalStringRef = (__bridge_retained CFStringRef)unencodedString;
    NSString *s = (__bridge_transfer NSString *)CFURLCreateStringByAddingPercentEscapes(NULL,originalStringRef, NULL, NULL,kCFStringEncodingUTF8);
    CFRelease(originalStringRef);
    return s;
}

+(NSString*)addQueryStringWithDictionary:(NSDictionary *)dictionary
{
    NSMutableString *urlWithQuerystring = [[NSMutableString alloc] init];
    
    for (id key in dictionary) {
        NSString *keyString = [key description];
        NSString *valueString = [[dictionary objectForKey:key] description];
        
    
            [urlWithQuerystring appendFormat:@"/%@=%@", [self urlEscapeString:keyString], [self urlEscapeString:valueString]];
    }
    return urlWithQuerystring;
}

+(NSDictionary *)parseJsonResponse:(NSString *)urlString
{
    NSError *error;
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse: nil error:&error];
    if (!data)
    {
        NSLog(@"Download Error: %@", error.localizedDescription);
        //[[[UIAlertView alloc]initWithTitle:@"Error" message:[NSString stringWithFormat:@"Error : %@",error.localizedDescription] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        return nil;
    }
    
    // Parse the (binary) JSON data from the web service into an NSDictionary object
    NSDictionary *JSON =  [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &error];
    return JSON;
}

@end

//
//  Singleton.h
//  GRADUMATE
//
//  Created by Ahmad Jarray on 20/11/2015.
//  Copyright © 2015 Ahmad Jarray. All rights reserved.
//

#import <Reachability/Reachability.h>
#import <MMDrawerController/MMDrawerController.h>
@class Bubble;
@interface Singleton : NSObject{
    Reachability *internetReachability;

}
+ (instancetype)sharedInstance;


-(NSArray*)commentsForBubbleID:(int)bubbleID Locally:(BOOL)isLocal;

-(NSMutableArray*)bubbleWithSearshKey:(NSString*)key;
-(NSMutableArray*)myTimeLine;
-(NSMutableArray*)BubblesWithParentID:(NSNumber*)pid;
-(int)getMaxID;

-(void)updateDateForBubbleWithID:(NSNumber*)bubbleID withDate:(NSDate*)date;
-(void)logout;

@property (nonatomic,retain)MMDrawerController *drawerController;
@property (nonatomic,retain)UIImage *image;
-(void)deleteBubbleWithID:(NSNumber*)timeLineId;
-(NSMutableArray*)timeLineForUser:(NSNumber*)userID;
-(void)updateBubblesFromArray:(NSArray*)data forUserID:(NSNumber*)userId;
-(void)saveBubble:(Bubble*)bubble withID:(NSNumber*)ID;
-(void)UpdateParentId:(NSNumber*)pID withID:(NSNumber*)ID;
-(void)updateLinkedBubbleWithID:(NSNumber*)bubbleID withParentId:(NSNumber*)parentId;
@end

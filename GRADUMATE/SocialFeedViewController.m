//
//  SocialFeedViewController.m
//  GRADUMATE
//
//  Created by Malek Mansour on 7/4/15.
//  Copyright (c) 2015 SlickStone. All rights reserved.
//

#import "SocialFeedViewController.h"
#import "AppDelegate.h"
#import "SOCIALFEED.h"
#import "CERTViewController.h"
#import "SocialFeedTableCell.h"
#import "CommentViewController.h"
#import "JSONHelper.h"
#import <FMDB/FMDatabase.h>

@interface SocialFeedViewController (){
    UIRefreshControl * refreshControl;
    long WeekNumber;
    NSMutableArray *list;
}

@property (nonatomic) Reachability *internetReachability;

@end

@implementation SocialFeedViewController
@synthesize timeline;

- (void)viewDidLoad
{
    [super viewDidLoad];
    table.estimatedRowHeight = 150.0;
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refreshCallback) forControlEvents:UIControlEventValueChanged];
    [table addSubview:refreshControl];
    WeekNumber=1;
    [self updateTable];
    
  
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void) refreshCallback
{
    self.internetReachability = [Reachability reachabilityForInternetConnection];
    [self.internetReachability startNotifier];
    NetworkStatus netStatus = [self.internetReachability currentReachabilityStatus];
    if (netStatus==NotReachable)
    {
        [[[UIAlertView alloc] initWithTitle:nil message:@"No Internet Connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
    else
    {
        WeekNumber++;
        [refreshControl endRefreshing];
        [self updateTable];
        [table reloadData];
    }
}

-(void)Save_COMMENT:(NSMutableArray *)LIST_TO_SAVE
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"GRADUMATE.sqlite"];
    FMDatabase* db = [FMDatabase databaseWithPath:writableDBPath];
    
    if(![db open])
    {
        NSLog(@"Err %d: %@",[db lastErrorCode],[db lastErrorMessage]);
    }
    
    [db setShouldCacheStatements:YES];
}

- (IBAction) ShowSideMenu:(id)sender
{
    [[Singleton sharedInstance].drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
    
}
-(void)updateTable
{
    self.internetReachability = [Reachability reachabilityForInternetConnection];
    [self.internetReachability startNotifier];
    
    NetworkStatus netStatus = [self.internetReachability currentReachabilityStatus];
    if (netStatus==NotReachable)
    {

    }
    else
    {
           list = [[NSMutableArray alloc] init];
        [JSONHelper getFeedForUserId:[[[[NSUserDefaults standardUserDefaults] objectForKey:@"GRAD_USER"] objectForKey:@"ID"] intValue] inPage:0 delegate:self tag:0];
    }
}
-(void)receiveData:(NSMutableArray*)LISTS tag:(int)tag{
    if ([LISTS count]>0)
    {
        for (NSDictionary * dic in LISTS)
        {
            [list addObject:[SOCIALFEED feedFromDic:dic]];
        }
        [table reloadData];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [list count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
    // return 6;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
 
        return   UITableViewAutomaticDimension;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     SocialFeedTableCell *cell = (SocialFeedTableCell *)[tableView dequeueReusableCellWithIdentifier:@"SocialFeedTableCell"];
    
    SOCIALFEED *SF = [list objectAtIndex:indexPath.section];
    [cell configWithFeed:SF];
 
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
        if ([segue.identifier isEqualToString:@"comment"]) {
            
            NSIndexPath *indexPath=[table indexPathForCell:sender];
            SOCIALFEED *f=[list objectAtIndex:indexPath.section];
            ( (CommentViewController*)segue.destinationViewController ).feed=f;
           
        }
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

@end

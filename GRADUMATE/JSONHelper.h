//
//  JSONHelper.h
//
//
//  Created by Malek Mansour on 08/06/14.
//  Copyright (c) 2014 Lekma. All rights reserved.
//


@protocol helperDelegate <NSObject>

-(void)receiveData:(id)data;
-(void)receiveData:(id)data tag:(int)tag;
-(void)hideHud;
//-(void)receiveUsersData:(id)responseData tag:(int)tag;


@end

@interface JSONHelper : NSObject
+(void)parseJsonResponse:(NSString *)urlString withDelegate:(id<helperDelegate>)delegate;
+(NSDictionary *)parseJsonResponse:(NSString *)urlString;
+(void)getDataPostFrom:(NSString *)urlString postData:(NSDictionary*)postdic delegate:(id<helperDelegate>)delegate tag:(int)tag;
+(void)getTimeLineforUserId:(NSNumber*)userId delegate:(id<helperDelegate>)delegate tag:(int)tag;
+(void)getFeedForUserId:(int)userId inPage:(int)page delegate:(id<helperDelegate>)delegate tag:(int)tag;
+(void)getCommentsForShareId:(int)shareId inPage:(int)page delegate:(id<helperDelegate>)delegate tag:(int)tag;
+(void)addComments:(NSString*)comment ForShareId:(int)shareId atIndex:(int)page delegate:(id<helperDelegate>)delegate tag:(int)tag;

+(void)getUsersDelegate:(id<helperDelegate>)delegate tag:(int)tag;

+(void)getMyTimeLineDelegate:(id<helperDelegate>)delegate tag:(int)tag;
+(void)saveBubble:(Bubble*)bubble withImage:(UIImage*)image delegate:(id<helperDelegate>)delegate tag:(int)tag;
+(void)CloneFeedWithId:(NSInteger*)bubbleID ForUserWithId:(NSInteger*)userId delegate:(id<helperDelegate>)delegate tag:(int)tag;
+(void)GetListUsersWithId:(NSNumber*)userID delegate:(id<helperDelegate>)delegate tag:(int)tag;
+(void)ShareBubble:(Bubble*)bubble title:(NSString*)title withUsers:(NSString*)users delegate:(id<helperDelegate>)delegate tag:(int)tag;

@end

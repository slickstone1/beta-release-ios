//
//  UserCell.m
//  GRADUMATE
//
//  Created by Ahmad Jarray on 10/12/2015.
//  Copyright © 2015 Ahmad Jarray. All rights reserved.
//

#import "UserCell.h"

#import "User.h"

#import <SDWebImage/UIImageView+WebCache.h>
@implementation UserCell

-(void)awakeFromNib{
    image.layer.masksToBounds=YES;
    labelName.textColor=PRINCIPAL_BORDER_COLOR;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        // Initialization code
    }
    return self;
}
-(void)configWithUser:(NSDictionary*)user{

    [image sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[user objectForKey:@"IMAGE"] ] ]placeholderImage:[UIImage imageNamed:@"58"]];
    
    labelName.text=[user objectForKey:@"NAME"];
}

 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 image.layer.cornerRadius=image.frame.size.height/2;
 
 // Drawing code
 }

@end

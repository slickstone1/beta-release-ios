//
//  CERTViewController.h
//  GRADUMATE
//
//  Created by Malek Mansour on 8/14/15.
//  Copyright © 2015 SlickStone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CERTViewController : UIViewController
{
    IBOutlet UIImageView *img;
}


@property(nonatomic, readwrite) NSString *NAME;
@property(nonatomic, readwrite) NSString *LINK;
@end

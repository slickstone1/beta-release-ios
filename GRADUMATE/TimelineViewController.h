//
//  LifeLineViewController.h
//  GRADUMATE
//
//  Created by Jarray on 10/11/2015.
//  Copyright © 2015 SlickStone. All rights reserved.
//


#import "YALContextMenuTableView.h"
#import <DragAndDropTableView/DragAndDropTableView.h>
@class User;
@interface TimelineViewController : UIViewController <UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,UITabBarDelegate,UITabBarControllerDelegate,UIScrollViewDelegate,YALContextMenuTableViewDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate>

{
    
    IBOutlet DragAndDropTableView *tLTable;
    IBOutlet YALContextMenuTableView* contextMenuTableView;
    IBOutlet UITableView*usersTable;
    IBOutlet UIButton *CreateBubbleBtn;
    IBOutlet UIButton *SettingsBtn1;
    IBOutlet UIButton *backBtn;
    IBOutlet NSLayoutConstraint *seqrshHeight;
    IBOutlet UIView *EditContainer;
    IBOutlet UIButton *EditBtn;
    IBOutlet  UIButton *SearchBtn;
    UIButton *SearchBtn2;
    IBOutlet  UIImageView *LINE;
    IBOutlet UITextField *SearchBAR,*bubbleTitle;
    IBOutlet   UIDatePicker *DATEEdit_Mode;
    IBOutlet UIView*cornerView,*usersView;
    IBOutlet UIButton * addBTN;
    IBOutlet NSLayoutConstraint *menuWidth;
    
    BOOL canMove;
    NSArray *menuTitles;
    NSArray  *menuIcons;
    NSMutableArray *TimeLinelist,*listUsers,*usersSelected;
    long MyUserID;

}

@property (strong, nonatomic) User *user;

-(IBAction)ShowSearch:(UIButton*)sender;
-(IBAction)EditTimeLine:(id)sender;
-(IBAction)SaveAfterEditMode:(id)sender;
-(IBAction)CancelEditMode:(id)sender;
-(IBAction)SEARCH:(id)sender;
-(IBAction) ShowSideMenu:(id)sender;

@end



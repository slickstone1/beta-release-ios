//
//  RegisterViewController.m
//  GRADUMATE
//
//  Created by Malek Mansour on 3/31/15.
//  Copyright (c) 2015 SlickStone. All rights reserved.
//

#import "RegisterViewController.h"
#import <AFNetworking/AFHTTPSessionManager.h>
#import <CommonCrypto/CommonCrypto.h>
#import "FMDatabase.h"
#import "JSONHelper.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"


@interface RegisterViewController (){
    MBProgressHUD*hud;
}

@end

@implementation RegisterViewController

//- (void)textFieldDidBeginEditing:(UITextField *)textField {
//    [MAINSCROLL setScrollEnabled:YES];
//    CGPoint scrollPoint = CGPointMake(0, 95);
//    [MAINSCROLL setContentOffset:scrollPoint animated:YES];
//}
//
//- (void)textFieldDidEndEditing:(UITextField *)textField {
//    [MAINSCROLL setScrollEnabled:NO];
//    [MAINSCROLL setContentOffset:CGPointZero animated:YES];
//}


-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    activeField=textField;
    
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    
    return NO;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    
    
//    [MAINSCROLL setScrollEnabled:NO];
    
//    [FIRST setDelegate:self];
//    [SUR setDelegate:self];
//    [MAIL setDelegate:self];
//    [SKYPE setDelegate:self];
//    [PWD setDelegate:self];
    
    // Do any additional setup after loading the view.
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
//    [self.view addGestureRecognizer:tap];
    [self registerForKeyboardNotifications];
    
    [self.view setNeedsUpdateConstraints];
    [self.view setNeedsLayout];
    self.view.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    MAINSCROLL.contentInset = contentInsets;
    MAINSCROLL.scrollIndicatorInsets = contentInsets;
    
    [MAINSCROLL scrollRectToVisible:activeField.frame animated:YES];
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    MAINSCROLL.contentInset = contentInsets;
    MAINSCROLL.scrollIndicatorInsets = contentInsets;
}


- (void)viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}
- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)SIGNUP:(id)sender
{
    if ([FIRST.text isEqualToString:@""]&&[FIRST.text isEqualToString:@""]&&[FIRST.text isEqualToString:@""]&&[FIRST.text isEqualToString:@""]&&[FIRST.text isEqualToString:@""])
    {
        [[[UIAlertView alloc] initWithTitle:nil message:@"Would you please fill all requirements" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
    else
    {
//        [self dismissKeyboard];
//        NSString *F = [[NSString alloc] initWithFormat:@"%@",[FIRST.text stringByReplacingOccurrencesOfString:@" " withString:@"%20"]];
//        NSString *S = [[NSString alloc] initWithFormat:@"%@",[SUR.text stringByReplacingOccurrencesOfString:@" " withString:@"%20"]];
//        NSString *M = [[NSString alloc] initWithFormat:@"%@",[MAIL.text stringByReplacingOccurrencesOfString:@" " withString:@"%20"]];
//        NSString *P = [[NSString alloc] initWithFormat:@"%@",[PWD.text stringByReplacingOccurrencesOfString:@" " withString:@"%20"]];
        
        NSString *F=FIRST.text;
        NSString *S=SUR.text;
        NSString *M=MAIL.text;
        NSString *P=[self md5String:PWD.text];
        
        //NSString *SKY = [[NSString alloc] initWithFormat:@"%@",[SKYPE.text stringByReplacingOccurrencesOfString:@" " withString:@"%20"]];
        
        //if(!SKY)
        //    SKY=@"";
        
        NSString *a ;//= [((AppDelegate *)[UIApplication sharedApplication].delegate).DEVICEToken stringByReplacingOccurrencesOfString:@"<" withString:@""];
        NSString *b = [a stringByReplacingOccurrencesOfString:@">" withString:@""];
        b=[b stringByReplacingOccurrencesOfString:@" " withString:@""];
        if(!b)
            b=@"SIMULATOR";
        
        UIImage *img=[UIImage imageNamed:@"dX.png"];
        NSString *I=[UIImagePNGRepresentation(img) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
        NSDictionary* jsondic = [NSDictionary dictionaryWithObjects:@[F,S,P,M,b] forKeys:@[@"NAME",@"SURNAME",@"PWD",@"EMAIL",@"DEVICE"]];
        
        hud=[[MBProgressHUD alloc]init];
        [self.view addSubview:hud];
        [hud show:YES];
        
        [JSONHelper getDataPostFrom:@"Registration" postData:jsondic delegate:self tag:0];
    }
}

-(void)hideHud{
     [hud hide:YES];
}

- (NSString *)md5String:(NSString*)strig
{
    const char *cstr = [strig UTF8String];
    unsigned char result[16];
    CC_MD5(cstr, strlen(cstr), result);
    
    return [NSString stringWithFormat:
            @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
}

//- (NSString *) md5:(NSString *)str {
//    const char *cStr = [str UTF8String];
//    unsigned char result[16];
//    CC_MD5( cStr, strlen(cStr), result );
//    return [NSString stringWithFormat:
//            @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
//            result[0], result[1], result[2], result[3],
//            result[4], result[5], result[6], result[7],
//            result[8], result[9], result[10], result[11],
//            result[12], result[13], result[14], result[15]
//            ];
//}


-(void)receiveData:(id)responseData tag:(int)tag{
    [hud hide:YES];
    if ([responseData isKindOfClass:[NSDictionary class]])
    {
        NSString *etat = [responseData valueForKey:@"ETAT"];
        NSLog(@"%@",etat);
        
        if ([etat isEqualToString:@"YES"])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Successfully Registered" message:@"Would you please check your email inbox to activate your account" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert setCancelButtonIndex:0];
            alert.tag=1;
            [alert show];
            [self dismissViewControllerAnimated:YES completion:nil];
            
        }else if([etat isEqualToString:@"Mail exist !"]){
            [[[UIAlertView alloc] initWithTitle:nil message:@"Email address already exists!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
        else
        {
            [[[UIAlertView alloc] initWithTitle:@"Some error occurred!" message:@"Would you please repeat" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==1)//Successfully Registered
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

//- (void) dismissKeyboard
//{
//    [FIRST resignFirstResponder];
//    [SUR resignFirstResponder];
//    [MAIL resignFirstResponder];
//    [PWD resignFirstResponder];
//    [SKYPE resignFirstResponder];
//}

-(IBAction)BACK:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end

//
//  LogOnViewController.m
//  GRADUMATE
//
//  Created by Malek Mansour on 3/24/15.
//  Copyright (c) 2015 SlickStone. All rights reserved.
//

#import "LogOnViewController.h"
#import "JSONHelper.h"
#import <FMDB/FMDB.h>
#import "GHWalkThroughView.h"
#import <CommonCrypto/CommonCrypto.h>


@interface LogOnViewController ()<GHWalkThroughViewDataSource>
@property (nonatomic, strong) GHWalkThroughView* ghView ;
@property (nonatomic, strong) NSArray* descStrings;

@property (weak, nonatomic) IBOutlet UIButton *authenticationButton;

@end

@implementation LogOnViewController

static float progress;



-(void)Next
{
//    [tumblrHUD removeFromSuperview];
    [LOGINT setHidden:NO];
    [PASSWORDT setHidden:NO];
    [LOGINB setHidden:NO];
    [SIGNUPB setHidden:NO];
}


-(void)Animate
{
    /*
    [tumblrHUD showAnimated:YES];
    progress+=1.0f;
    if(progress < 5.0f)
        [self performSelector:@selector(Animate) withObject:nil afterDelay:0.1];
    else
     */
        [self performSelector:@selector(Next) withObject:nil afterDelay:0.1];
}



-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    LOGINT.text =@"";
    PASSWORDT.text = @"";
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];

    

}


- (void)viewWillDisappear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}
- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    loginView.layer.cornerRadius=loginView.frame.size.height/2;
    passwordView.layer.cornerRadius=passwordView.frame.size.height/2;
    
    self.navigationController.delegate = self;
    [LOGINT setHidden:YES];
    [PASSWORDT setHidden:YES];
    [LOGINB setHidden:YES];
    [SIGNUPB setHidden:YES];
    
    LOGINT.delegate=self;
    PASSWORDT.delegate=self;
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    progress = 0.0f;
//    tumblrHUD = [[AMTumblrHud alloc] initWithFrame:CGRectMake((CGFloat) ((self.view.frame.size.width - 55) * 0.5),(CGFloat) ((self.view.frame.size.height - 20) * 0.5), 55, 20)];
//    tumblrHUD.hudColor = UIColorFromRGB(0x343434);
//    [self.view addSubview:tumblrHUD];
    [self Animate];
    
    //if first launch
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"TutoOnce"])
        ;
    else
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"TutoOnce"];
        [self showtuto];
    }
    
    [self.view setNeedsUpdateConstraints];
    [self.view setNeedsLayout];
    self.view.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
   
    return YES;
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    return YES;
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    
//    [UIView animateWithDuration:.2f animations:^{
//        CGRect theFrame = CGRectMake(0,-60,320,[[UIScreen mainScreen] bounds].size.height);
//        self.view.frame = theFrame;
//    }];
    
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    
//    [UIView animateWithDuration:.2f animations:^{
//        CGRect theFrame = CGRectMake(0,0,320,[[UIScreen mainScreen] bounds].size.height);
//        self.view.frame = theFrame;
//    }];
}

- (void) showtuto
{
    _ghView = [[GHWalkThroughView alloc] initWithFrame:self.navigationController.view.bounds];
    [_ghView setDataSource:self];
    [_ghView setWalkThroughDirection:GHWalkThroughViewDirectionVertical];
    self.descStrings = [NSArray arrayWithObjects:NSLocalizedString(@"Tuto1", nil),
                        NSLocalizedString(@"Tuto2", nil),
                        NSLocalizedString(@"Tuto3",nil),
                        NSLocalizedString(@"Tuto4", nil),
                        NSLocalizedString(@"Tuto5", nil), nil];
    
    
    
    self.ghView.floatingHeaderView = nil;
    [self.ghView setWalkThroughDirection:GHWalkThroughViewDirectionHorizontal];
    [self.ghView showInView:self.navigationController.view animateDuration:0.3];
    
}
#pragma mark - GHDataSource

-(NSInteger) numberOfPages
{
    return [self.descStrings count];
}

- (void) configurePage:(GHWalkThroughPageCell *)cell atIndex:(NSInteger)index
{
    cell.title = [NSString stringWithFormat:@""];
    cell.titleImage = [UIImage imageNamed:[NSString stringWithFormat:@"title%ld", index+1]];
    cell.accessibilityHint=[NSString stringWithFormat:@"%ld",(long)index];
    cell.tag=index;
    cell.desc = [self.descStrings objectAtIndex:index];
   
    ;
}

- (UIImage*) bgImageforPage:(NSInteger)index
{
    NSString* imageName =[NSString stringWithFormat:@"presentation p%ld.png", index+1];
    UIImage* image = [UIImage imageNamed:imageName];
    return image;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)next:(id)sender
{
    
}

-(void )PresentTAB
{

    MMDrawerController *drawerController=[self.storyboard instantiateViewControllerWithIdentifier:@"main"];
    [drawerController setShowsShadow:YES];
    
    [drawerController setMaximumRightDrawerWidth:280];
    [drawerController setMaximumLeftDrawerWidth:280];
    [drawerController setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [drawerController setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
    
    self.navigationController.viewControllers=@[drawerController];
    
    [Singleton sharedInstance].drawerController=drawerController;
}
//-(void)receiveData:(id)responseData{
//   }
-(IBAction)LOGIN:(id)sender
{

    [self dismissKeyboard];
    
    NSDictionary* jsondic = [NSDictionary dictionaryWithObjects:@[LOGINT.text,[self md5String:PASSWORDT.text]] forKeys:@[@"login",@"pwd"]];
    
  //   NSString*postString=[NSString stringWithFormat:@"login=%@&pwd=%@",LOGINT.text,PASSWORDT.text];
    [JSONHelper getDataPostFrom:@"JsonLogin" postData:jsondic delegate:self tag:0];
}

-(void)receiveData:(id)responseData tag:(int)tag{
    if ([responseData isKindOfClass:[NSDictionary class]])
    {
        NSString *etat = [responseData valueForKey:@"ETAT"];
        if ([etat isEqual:@"NO"])
        {
            [[[UIAlertView alloc] initWithTitle:@"Error" message:@"Your login and password don't match" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil] show];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setObject:responseData forKey:@"GRAD_USER"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self PresentTAB];
        }
    }
    else
    {
        [[[UIAlertView alloc]initWithTitle:@"Error" message:@"No data received from server" delegate:self cancelButtonTitle:@"Verify Internet Connection" otherButtonTitles:nil, nil] show];
    }

}

- (NSString *)md5String:(NSString*)strig
{
    const char *cstr = [strig UTF8String];
    unsigned char result[16];
    CC_MD5(cstr, strlen(cstr), result);
    
    return [NSString stringWithFormat:
            @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
}



-(void)dismissKeyboard
{
    [LOGINT resignFirstResponder];
    [PASSWORDT resignFirstResponder];
}


-(void) presentAlertControllerWithMessage:(NSString *) message
{
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"Touch ID" message:message preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
    [self presentViewController:alertController animated:YES completion:nil];
}


 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }





@end

//
//  SimpleTableCell.h
//  SimpleTable
//
//  Created by Simon Ng on 28/4/12.
//  Copyright (c) 2012 Appcoda. All rights reserved.
//
#import "MGSwipeTableCell.h"

@class SOCIALFEED;
@interface SocialFeedTableCell : MGSwipeTableCell{
   IBOutlet UILabel *usernameLab;
   IBOutlet UILabel *actionLab;
    IBOutlet UILabel *bubbleLab;
    IBOutlet UILabel *contentLab;
   IBOutlet UILabel *dateLab;
   IBOutlet UIImageView *thumbnailImageView;
   IBOutlet UIImageView *KindImageView;

}

-(void)configWithFeed:(SOCIALFEED*)feed;

@end

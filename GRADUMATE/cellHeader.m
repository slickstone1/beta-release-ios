//
//  cellHeader.m
//  GRADUMATE
//
//  Created by Ahmad Jarray on 10/12/2015.
//  Copyright © 2015 Ahmad Jarray. All rights reserved.
//

#import "cellHeader.h"

@implementation cellHeader
-(void)awakeFromNib{
    titleSection.textColor=PRINCIPAL_BORDER_COLOR;
}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */
@end

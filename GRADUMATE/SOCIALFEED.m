//
//  SOCIALFEED.m
//  GRADUMATE
//
//  Created by Malek Mansour on 8/13/15.
//  Copyright © 2015 SlickStone. All rights reserved.
//

#import "SOCIALFEED.h"

@implementation SOCIALFEED

+(instancetype)feedFromDic:(NSDictionary*)dic{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss.SSS"];

    
    SOCIALFEED *SF = [[SOCIALFEED alloc] init];
    SF.NAME_ITEM = [dic valueForKey:@"BUBBLE"];
    SF.NAME = [dic valueForKey:@"NAME"];
    SF.TEXT = [dic valueForKey:@"TITLE_SHARE"];
    SF.KIND = [dic valueForKey:@"KIND_BUBBLE"];
    SF.ID = [[dic valueForKey:@"ID_SOCIAL"] integerValue];
    SF.ID_ITEM = [[dic valueForKey:@"ID_BUBBLE"] integerValue];
    SF.userImage = [dic valueForKey:@"LINK"];
    
    SF.ID_USERSENDER = [[dic valueForKey:@"ID_SENDER"] integerValue];
    SF.DATE =  [dateFormat dateFromString:[dic valueForKey:@"DATE_SOCIAL"]];
    
    return SF;
}


@end

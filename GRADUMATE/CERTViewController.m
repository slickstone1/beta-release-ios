//
//  CERTViewController.m
//  GRADUMATE
//
//  Created by Malek Mansour on 8/14/15.
//  Copyright © 2015 SlickStone. All rights reserved.
//

#import "CERTViewController.h"

@interface CERTViewController ()

@end

@implementation CERTViewController
@synthesize LINK,NAME;




- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationController setNavigationBarHidden:NO];
    
    NSString *TIT = [[NSString alloc] initWithFormat:@"%@ get a certificate",NAME];
    [self.navigationController setTitle:TIT];
    
    NSString *RE = [LINK stringByReplacingOccurrencesOfString:@"'\'" withString:@""];
    
    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:RE]]];
    img.image = image;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

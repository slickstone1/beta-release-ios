//
//  TLTableViewController.h
//  GRADUMATE
//
//  Created by Malek Mansour on 6/22/15.
//  Copyright (c) 2015 SlickStone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Bubble.h"

@interface TLTableViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
    
    NSMutableArray *list;
    IBOutlet UITableView *table;
    
    IBOutlet UITextField *SearchBAR;
    
    IBOutlet UIButton *MENU;
    IBOutlet UIButton *SettingsBtn1;
    IBOutlet UIImageView *USER_PHOTO;

    long MINE;

}
@property (strong, nonatomic) id delegate;

@property (strong, nonatomic) IBOutlet UIImageView *BarImg;
@property (strong, nonatomic) IBOutlet UIButton *SearchBtn;
@property (strong, nonatomic) IBOutlet UIButton *SearchBtn2;

@property (strong, nonatomic) IBOutlet UIView *centerView;
@property (strong, nonatomic) IBOutlet UIView *sideView;

@property (strong, nonatomic) IBOutlet UIButton *SettingsBtn1;
@property (strong, nonatomic) UIWindow *window;
@end

@interface DashCell : UITableViewCell


@property (retain, nonatomic) IBOutlet UIButton *CheckBtn;
@property (retain, nonatomic) IBOutlet UILabel *TitleLab;
@property (retain, nonatomic) IBOutlet UILabel *DateLab;
@end



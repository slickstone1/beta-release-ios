//
//  BaseViez.m
//  GRADUMATE
//
//  Created by Ahmad Jarray on 08/12/2015.
//  Copyright © 2015 Ahmad Jarray. All rights reserved.
//

#import "BaseView.h"

@implementation BaseView

- (void) awakeFromNib
{
    self.backgroundColor=PRINCIPAL_TINT_COLOR;
}

@end

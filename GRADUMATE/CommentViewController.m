//
//  CommentViewController.m
//  GRADUMATE
//
//  Created by Ahmad Jarray on 04/12/2015.
//  Copyright © 2015 Ahmad Jarray. All rights reserved.
//

#import "CommentViewController.h"
#import "CommentTableCell.h"
#import "SOCIALFEED.h"
#import "COMMENT.h"
#import "JSONHelper.h"

@interface CommentViewController (){
    UIRefreshControl * refreshControl;
    long WeekNumber;
    NSMutableArray *list;
    
}

@end

@implementation CommentViewController

-(IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    table.estimatedRowHeight=110;
    list=[NSMutableArray array];
    bubbleTitle.text=_feed.NAME_ITEM;
    bubbleComment.text=_feed.TEXT;
     [JSONHelper getCommentsForShareId:_feed.ID_ITEM inPage:0 delegate:self tag:0];
    
      
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)keyboardWillHide:(NSNotification *)n
{
    keyboardConst.constant=0;
    
}

- (void)keyboardWillShow:(NSNotification *)n
{
    NSDictionary* userInfo = [n userInfo];
    
    // get the size of the keyboard
    CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;

    
     keyboardConst.constant=keyboardSize.height;
}

-(void)receiveData:(NSMutableArray*)LISTS tag:(int)tag{
    if (tag==0) {
        if ([LISTS count]>0)
        {
            for (NSDictionary * dic in LISTS)
            {
                [list addObject:[COMMENT commentFromDic:dic]];
            }
            [table reloadData];
            
             [table scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[LISTS count]-1 inSection:0] atScrollPosition:UITableViewScrollPositionNone animated:YES];
        }
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
        return   UITableViewAutomaticDimension;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [list count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    COMMENT *comment=[list objectAtIndex:indexPath.row];
    CommentTableCell *cell;
     if ([comment.ID_USERRECEIVER intValue]==[[[[NSUserDefaults standardUserDefaults] objectForKey:@"GRAD_USER"] objectForKey:@"ID"] intValue]) {
        cell=[tableView dequeueReusableCellWithIdentifier:@"me"];
       
    }else
         cell=[tableView dequeueReusableCellWithIdentifier:@"ot"];
    cell.comment= comment;

    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
}

-(IBAction)Send:(id)sender{
    if ([commentLabel.text isKindOfClass:[NSString class]] && [[commentLabel.text stringByReplacingOccurrencesOfString:@" " withString:@""] length]>0) {
        [list addObject:[COMMENT newCommentwithText:commentLabel.text]];
        [JSONHelper addComments:commentLabel.text ForShareId:_feed.ID_ITEM atIndex:list.count delegate:self tag:1];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:list.count-1 inSection:0];
        [table beginUpdates];
        [table insertRowsAtIndexPaths: @[indexPath]withRowAnimation:UITableViewRowAnimationBottom];
        [table endUpdates];
        commentLabel.text=@"";
        [table scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionNone animated:YES];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

@end

//
//  SimpleTableCell.h
//  SimpleTable
//
//  Created by Simon Ng on 28/4/12.
//  Copyright (c) 2012 Appcoda. All rights reserved.
//

@class COMMENT;
@interface CommentTableCell : UITableViewCell{
    IBOutlet  UILabel *dateLab;
    IBOutlet  UIImageView *UserImg;
    IBOutlet  UILabel *Username;
    IBOutlet  UILabel *commentLabel;
    IBOutlet UIView *messageContnerView;
    
}
@property (nonatomic,retain) COMMENT* comment;


@end

//
//  RegisterViewController.h
//  GRADUMATE
//
//  Created by Malek Mansour on 3/31/15.
//  Copyright (c) 2015 SlickStone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterViewController : UIViewController <UITextFieldDelegate,UIAlertViewDelegate>
{
    IBOutlet UITextField *FIRST;
    IBOutlet UITextField *SUR;
    IBOutlet UITextField *MAIL;
    IBOutlet UITextField *PWD;
    IBOutlet UITextField *SKYPE;
    IBOutlet UIScrollView *MAINSCROLL;
    
    UITextField*activeField;

}


@end

//
//  MenuViewController.h
//  GRADUMATE
//
//  Created by Ahmad Jarray on 26/11/2015.
//  Copyright © 2015 Ahmad Jarray. All rights reserved.
//

@interface MenuViewController : UIViewController{
    IBOutlet UIImageView *userImage;
    IBOutlet UITableView *table;
    IBOutlet UILabel*name;
    IBOutlet UILabel*location;
}


-(IBAction)changeImage:(id)sender;

@end

//
//  Singleton.m
//  GRADUMATE
//
//  Created by Ahmad Jarray on 20/11/2015.
//  Copyright © 2015 Ahmad Jarray. All rights reserved.
//


#import "FMDatabase.h"
#import "JSONHelper.h"

#import "SVProgressHUD.h"

#import "COMMENT.h"
#import "Bubble.h"



@implementation Singleton

+ (instancetype)sharedInstance
{
    static Singleton *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[Singleton alloc] init];
        
    
        
        
        // Do any other initialisation stuff here
    });
    return sharedInstance;
}

-(id)init{
   
    internetReachability = [Reachability reachabilityForInternetConnection];
    [internetReachability startNotifier];
    return self;
}

-(int)getMaxID{
    return [self getPreviousID:999999999];
}

-(int)getPreviousID:(int)param{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"GRADUMATE.sqlite"];
    FMDatabase* db = [FMDatabase databaseWithPath:writableDBPath];
    if(![db open])
    {
        NSLog(@"Err %d: %@",[db lastErrorCode],[db lastErrorMessage]);
    }
    [db setShouldCacheStatements:YES];
    
    NSString*  sql;
    sql = [[NSString alloc] initWithFormat:@"SELECT ID FROM GRAD_COMMENT WHERE ID=%d",param];
    FMResultSet*    rs = [db executeQuery:sql];
    NSMutableArray*list=[NSMutableArray array];
    while([rs next])
        [list addObject:rs];
    if ([list count]==0) {
        return param;
    }else
        return [self getPreviousID:param-1];
}

-(NSArray*)commentsForBubbleID:(int)bubbleID Locally:(BOOL)isLocal{
    NSMutableArray *commentsList=[[NSMutableArray alloc] init];
    
    if (([self ReachabilityStatus]==NotReachable)||(isLocal==1))
    {
        NSLog(@"there is not internet");
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"GRADUMATE.sqlite"];
        FMDatabase* db = [FMDatabase databaseWithPath:writableDBPath];
        if(![db open])
        {
            NSLog(@"Err %d: %@",[db lastErrorCode],[db lastErrorMessage]);
        }
        [db setShouldCacheStatements:YES];
        
        NSString*  sql;
        sql = [[NSString alloc] initWithFormat:@"SELECT* FROM GRAD_COMMENT WHERE ID_ITEM=%@",[NSNumber numberWithLong:bubbleID]];
        FMResultSet*    rs = [db executeQuery:sql];
        commentsList = [[NSMutableArray alloc] init];
        COMMENT *CCC;
        
        while([rs next])
        {
            CCC = [[COMMENT alloc] init];
            CCC.TEXT = [rs stringForColumn:@"TEXT"];
            CCC.NAME = [rs stringForColumn:@"NAME"];
            CCC.ID_ITEM = [rs intForColumn:@"ID_ITEM"];
            CCC.DATE = [rs dateForColumn:@"DATE"];
            
            // [commentsList addObject:[NSNumber numberWithInteger:1]];
            [commentsList addObject:CCC];
            CCC=nil;
        }
    }
    else
    {
        NSString *url = [[NSString alloc] initWithFormat:@"%@UserFeed?item=%d",@BASE_URL,bubbleID];
        NSDictionary *responseData = [JSONHelper parseJsonResponse:url];
        NSArray *LISTS = [responseData valueForKey:@"LIST"];
        commentsList = [[NSMutableArray alloc] init];
        if ([LISTS count]>0)
        {
            int iii;
            for (iii=0; iii<[LISTS count]; iii++)
            {
                NSLog(@"intenret i: %i",iii);
                COMMENT *COM;
                COM = [[COMMENT alloc] init];
                COM.ID_ITEM = [[[LISTS objectAtIndex:iii] valueForKey:@"ID_ITEM"] integerValue];
                COM.ID_USERSENDER = [[[LISTS objectAtIndex:iii] valueForKey:@"ID_USER"] integerValue];
                COM.NAME = [[LISTS objectAtIndex:iii] valueForKey:@"NAME"];
                COM.TEXT = [[LISTS objectAtIndex:iii] valueForKey:@"TEXT"];
                
                NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss.S"];
                COM.DATE =  [dateFormat dateFromString:[[NSString alloc] initWithFormat:@"%@",[[LISTS objectAtIndex:iii] valueForKey:@"DATE"]]];
                
                // [commentsList addObject:[NSNumber numberWithInteger:iii+1]];
                [commentsList addObject:COM];
            }
        }
    }
    //_sharedApp.first=0;
    [SVProgressHUD dismiss];
    return commentsList;

}

-(NSMutableArray*)bubbleWithSearshKey:(NSString*)key{
    long MyUserID;
    NSDictionary *user=[[NSUserDefaults standardUserDefaults] objectForKey:@"GRAD_USER"];
    if (user) {
         MyUserID=[[user objectForKey:@"ID"] intValue];
    }
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"GRADUMATE.sqlite"];
    FMDatabase* db = [FMDatabase databaseWithPath:writableDBPath];
    
    if(![db open])
    {
        NSLog(@"Err %d: %@",[db lastErrorCode],[db lastErrorMessage]);
    }
    
    [db setShouldCacheStatements:YES];
    
    NSString*  sql;
    sql = [[NSString alloc] initWithFormat:@"SELECT* FROM GRAD_TIMELINE WHERE IsAccepted=1 AND IDU=%ld AND NAME LIKE '%%%@%%'",MyUserID,key];
    
    FMResultSet*    rs = [db executeQuery:sql];
    
    NSMutableArray *list = [[NSMutableArray alloc] init];
    while([rs next])
        [list addObject:[Bubble tinelineForColumn:rs]];
    
    [db close];
    
    return  list;

}


-(NSMutableArray*)myTimeLine{
   
    
    long MyUserID;
    NSDictionary *user=[[NSUserDefaults standardUserDefaults] objectForKey:@"GRAD_USER"];
    if (user) {
        MyUserID=[[user objectForKey:@"ID"] intValue];
    }
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"GRADUMATE.sqlite"];
    FMDatabase* db = [FMDatabase databaseWithPath:writableDBPath];
    
    if(![db open])
    {
        NSLog(@"Err %d: %@",[db lastErrorCode],[db lastErrorMessage]);
    }
    
    [db setShouldCacheStatements:YES];
    
    NSString* sql = [[NSString alloc] initWithFormat:@"SELECT* FROM GRAD_TIMELINE WHERE IDU=%ld ORDER BY date(AchDATE)",MyUserID];
    
    FMResultSet*    rs = [db executeQuery:sql];
    
   NSMutableArray *TimeLinelist = [[NSMutableArray alloc] init];
    while([rs next])
        [TimeLinelist addObject:[Bubble tinelineForColumn:rs]];
    
    [db close];
    
    return TimeLinelist;

}

-(NSMutableArray*)BubblesWithParentID:(NSNumber*)pid{
    
    long MyUserID;
    NSDictionary *user=[[NSUserDefaults standardUserDefaults] objectForKey:@"GRAD_USER"];
    if (user) {
        MyUserID=[[user objectForKey:@"ID"] intValue];
    }
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"GRADUMATE.sqlite"];
    FMDatabase* db = [FMDatabase databaseWithPath:writableDBPath];
    
    if(![db open])
    {
        NSLog(@"Err %d: %@",[db lastErrorCode],[db lastErrorMessage]);
    }
    
    [db setShouldCacheStatements:YES];
    
    NSString* sql = [[NSString alloc] initWithFormat:@"SELECT* FROM GRAD_TIMELINE WHERE IDU=%ld AND PID=%@ ORDER BY date(AchDATE)",MyUserID,pid];
    
    FMResultSet*    rs = [db executeQuery:sql];
    
    NSMutableArray *TimeLinelist = [[NSMutableArray alloc] init];
    while([rs next])
        [TimeLinelist addObject:[Bubble tinelineForColumn:rs]];
    
    [db close];
    
    return TimeLinelist;
    
}

-(NSMutableArray*)timeLineForUser:(NSNumber*)userID{
  
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"GRADUMATE.sqlite"];
    FMDatabase* db = [FMDatabase databaseWithPath:writableDBPath];
    
    if(![db open])
    {
        NSLog(@"Err %d: %@",[db lastErrorCode],[db lastErrorMessage]);
    }
    
    [db setShouldCacheStatements:YES];
    
    NSString* sql=[[NSString alloc] initWithFormat:@"SELECT* FROM GRAD_TIMELINE WHERE IDU=%@ ORDER BY date(AchDATE)",userID];
    
    FMResultSet*    rs = [db executeQuery:sql];
    
    NSMutableArray *TimeLinelist = [[NSMutableArray alloc] init];
    while([rs next])
        [TimeLinelist addObject:[Bubble tinelineForColumn:rs]];
    
    [db close];
    
    return TimeLinelist;
    
}

-(void)updateBubblesFromArray:(NSArray*)data forUserID:(NSNumber*)userId{
    
    if ([data count]>0)
    {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"YYYY-MM-dd"];
        NSDate *todaysDate;
        todaysDate = [NSDate date];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"GRADUMATE.sqlite"];
        
        FMDatabase* db = [FMDatabase databaseWithPath:writableDBPath];
        if(![db open])
        {
            NSLog(@"Err %d: %@",[db lastErrorCode],[db lastErrorMessage]);
        }
        [db setShouldCacheStatements:YES];
        
        int iii=0;
        for (iii=0; iii<[data count]; iii++)
        {
            
            Bubble *Time = [Bubble bubbleFromDic:[data objectAtIndex:iii]];
            NSNumber *userID;
            if (userId) {
                userID=userId;
            }else
                userID=[[[NSUserDefaults standardUserDefaults] objectForKey:@"GRAD_USER"] objectForKey:@"ID"];
            
            [db executeUpdate:@"INSERT INTO GRAD_TIMELINE(ID,NAME,DESC,AchDATE,KIND,LINK,IsAccepted,IsCompleted,IsPublished,IDU,PID) VALUES(?,?,?,?,?,?,1,0,1,?,?)",Time.ID,Time.NAME,Time.DESC,Time.bubbleDate,Time.KIND,Time.image,userID,Time.PID];
            
            Time=nil;
        }
    }

}
-(void)UpdateParentId:(NSNumber*)pID withID:(NSNumber*)ID{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"GRADUMATE.sqlite"];
    
    FMDatabase* db = [FMDatabase databaseWithPath:writableDBPath];
    if(![db open])
    {
        NSLog(@"Err %d: %@",[db lastErrorCode],[db lastErrorMessage]);
    }
    [db setShouldCacheStatements:YES];
    
    [db executeUpdate:@"UPDATE GRAD_TIMELINE SET PID=? WHERE ID=?",pID,ID,nil];
}

-(void)saveBubble:(Bubble*)bubble withID:(NSNumber*)ID{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"GRADUMATE.sqlite"];
    
    FMDatabase* db = [FMDatabase databaseWithPath:writableDBPath];
    if(![db open])
    {
        NSLog(@"Err %d: %@",[db lastErrorCode],[db lastErrorMessage]);
    }
    [db setShouldCacheStatements:YES];
    
    NSDictionary*dic=[[NSUserDefaults standardUserDefaults] objectForKey:@"GRAD_USER"];
    NSNumber *userID=[dic objectForKey:@"ID"];
    
    [db executeUpdate:@"INSERT INTO GRAD_TIMELINE(ID,NAME,DESC,AchDATE,KIND,LINK,IsAccepted,IsCompleted,IsPublished,IDU) VALUES(?,?,?,?,?,?,1,0,1,?)",ID,bubble.NAME,bubble.DESC,bubble.bubbleDate,bubble.KIND,bubble.image,userID];
    
    
    
}

-(void)updateDateForBubbleWithID:(NSNumber*)bubbleID withDate:(NSDate*)date{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"GRADUMATE.sqlite"];
    FMDatabase* db = [FMDatabase databaseWithPath:writableDBPath];
    if(![db open])
    {
        NSLog(@"Err %d: %@",[db lastErrorCode],[db lastErrorMessage]);
    }
    [db executeUpdate:@"UPDATE GRAD_TIMELINE SET AchDate=? WHERE ID=?",date,bubbleID,nil];
}

-(void)updateLinkedBubbleWithID:(NSNumber*)bubbleID withParentId:(NSNumber*)parentId{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"GRADUMATE.sqlite"];
    FMDatabase* db = [FMDatabase databaseWithPath:writableDBPath];
    if(![db open])
    {
        NSLog(@"Err %d: %@",[db lastErrorCode],[db lastErrorMessage]);
    }
    [db executeUpdate:@"UPDATE GRAD_TIMELINE SET PID=? WHERE ID=?",parentId,bubbleID,nil];
}

-(void)logout{
    long MyUserID;
    NSDictionary *user=[[NSUserDefaults standardUserDefaults] objectForKey:@"GRAD_USER"];
    if (user) {
        MyUserID=[[user objectForKey:@"ID"] longValue];
    }
    
    NSString *url;
    NSDictionary *responseData;//
    
    url = [[NSString alloc] initWithFormat:@"%@NotConnect?ID=%ld",@BASE_URL,MyUserID];
    responseData = [JSONHelper parseJsonResponse:url];
    if (responseData)
    {
    }
    url = [[NSString alloc] initWithFormat:@"%@DeleteUserFeed?IDU=%ld",@BASE_URL,MyUserID];
    responseData = [JSONHelper parseJsonResponse:url];
    if (responseData)
    {
    }
    
//    DBInitViewController *INIT = [[DBInitViewController alloc] init];
//    [INIT forget];
    
    [[NSUserDefaults standardUserDefaults]  removeObjectForKey:@"GRAD_USER"];
    
    [Singleton sharedInstance].drawerController.navigationController.viewControllers=@[[[Singleton sharedInstance].drawerController.navigationController.storyboard instantiateViewControllerWithIdentifier:@"LOGON"]];
}

-(void)deleteBubbleWithID:(NSNumber*)timeLineId{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"GRADUMATE.sqlite"];
    FMDatabase* db = [FMDatabase databaseWithPath:writableDBPath];
    if(![db open])
    {
        NSLog(@"Err %d: %@",[db lastErrorCode],[db lastErrorMessage]);
    }
    
    [db setShouldCacheStatements:YES];
    [db executeUpdate:@"DELETE FROM GRAD_TIMELINE WHERE ID=?",timeLineId,nil];
    [db close];
}

-(NetworkStatus)ReachabilityStatus{
   return [internetReachability currentReachabilityStatus];
}

@end

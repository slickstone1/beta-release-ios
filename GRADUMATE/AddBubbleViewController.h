//
//  AddBubbleViewController.h
//  GRADUMATE
//
//  Created by Jarray on 10/11/2015.
//  Copyright © 2015 SlickStone. All rights reserved.
//

//#import "ANProgressStepperAttributes.h"

@class iCarousel;
@interface AddBubbleViewController : UIViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITextFieldDelegate,UITextViewDelegate>
{
    
    IBOutlet UIView *InfoContainerView;
    IBOutlet UILabel *DateLab,*nameOfBubble;
    IBOutlet UIImageView*chooseImage;
    IBOutlet UIDatePicker *Datepicker;
    IBOutlet UIView *InfoView;
    IBOutlet UIView *bubbleDescriptionView,*BubbleLinkedView,*bubblesListView;
    IBOutlet UITextField *BubbleName,*date;
    IBOutlet UITextView *BubbleDescription;
    IBOutlet iCarousel*carouselView;
    IBOutlet UITableView*table;
    IBOutlet UIButton*descriptBtn;
    IBOutlet UIScrollView*DESC_SCROLL;
    


}

@property(nonatomic,assign)  NSInteger BubbleType;
@property (nonatomic) Reachability *internetReachability;

-(IBAction)ShowInfo:(UIButton*)sender;
-(IBAction)SelectIcon:(UITapGestureRecognizer*)sender;
-(IBAction)SelectDate;
-(IBAction)ChangedDate;
-(IBAction)Infoback;
-(IBAction)InfoSave;
-(IBAction)IsAcheived:(UIButton*)sender;
-(IBAction)ShowBubbleFiles:(UIButton*)sender;
-(IBAction)CreateSubBubble;
-(IBAction)BubbleNotLinked;
-(IBAction)BubbleLinked;
-(IBAction)LinkMyBubble;
-(IBAction)EscapeFromLinking;
-(IBAction)ShowDescription;
-(IBAction)ShowLinkedBubbles;
-(IBAction)addBubble:(id)sender;


@end

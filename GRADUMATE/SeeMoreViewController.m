//
//  SeeMoreViewController.m
//  GRADUMATE
//
//  Created by Malek Mansour on 6/9/15.
//  Copyright (c) 2015 SlickStone. All rights reserved.
//

#import "SeeMoreViewController.h"
#import "UILabel+InPlaceEdit.h"
#import "KxMenu.h"
#import "UIImageView+draw.h"
#import <SGActionView/SGActionView.h>
#import "BubbleLinkedCell.h"

@interface SeeMoreViewController () <UIGestureRecognizerDelegate>{
    
    UITextField*activeField;
    NSArray*listUsers;
    NSMutableArray*usersSelected,*linkedBubbles,*InlinkedBubbles,*toAddArray,*toRemoveArray;
}
@end

@implementation SeeMoreViewController
@synthesize TimeObject,COMMENT;

- (void) viewDidLoad
{
    [super viewDidLoad];
//    
//    [NAMEL ipe_enableInPlaceEdit:nil];
//    [DESCL ipe_enableInPlaceEdit:nil];
    toAddArray=[NSMutableArray array];
    toRemoveArray=[NSMutableArray array];
    
    [self InitAchievementImage];
 
    ReloadTimeline=0;
    [self initDate];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)initDate{
    
    NSDate *NowDate=TimeObject.bubbleDate;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMM yy"];
    NSString *toDay=[[dateFormatter stringFromDate:NowDate] uppercaseString];
    NSRange boldedRange = NSMakeRange(3, toDay.length-6);
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:toDay];
    
    [attrString addAttribute:NSFontAttributeName
                       value:[UIFont systemFontOfSize:30.0]
                       range:boldedRange];
    
    [bubbleDate setAttributedText:attrString];

}

-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    if (REF_NUMBER==1)
    {
        [CongView setHidden:YES];
        CONGLABEL.text = [[NSString alloc] initWithFormat:@""];
        [CongView2 setHidden:YES];
        REF_NUMBER=0;
        
    }
    [CongView setHidden:YES];
    [CongView2 setHidden:YES];
    
    if (TimeObject.IsCompleted ==1) {
        [IsCompleted setOn:YES animated:YES];
        [AchievementImg drawRectWithdegree:0.99];
    }
    else
    {
        [IsCompleted setOn:NO animated:YES];
        int i=0;
        for (Bubble*b in _AllBubbles) {
            
            if ([b.PID doubleValue]==[TimeObject.ID doubleValue]){
                i++;
            }
        }
        
        float f=(float)i/[_AllBubbles count];
        [AchievementImg drawRectWithdegree:f];

    }
    if (TimeObject.IsPublished ==1)
    {
        [IsPublished setOn:YES animated:YES];
    }
    else
    {
        [IsPublished setOn:NO animated:YES];
    }
    if (TimeObject.ONTL ==1)
    {
        [ONTL setOn:YES animated:YES];
    }
    else
    {
        [ONTL setOn:NO animated:YES];
    }
    
    // Do any additional setup after loading the view
    //        [self UpdateComments];
    //
    //        [ScrollComments setHidden:NO];
//    
//    NSCalendar *calendar = [NSCalendar currentCalendar];
//    long year = [calendar component:NSCalendarUnitYear fromDate:TimeObject.bubbleDate];
//    long month = [calendar component:NSCalendarUnitMonth fromDate:TimeObject.bubbleDate];
//    long day = [calendar component:NSCalendarUnitDay fromDate:TimeObject.bubbleDate];
//    
//    //DATEL.text = [[NSString alloc] initWithFormat:@"%lu-%lu-%lu",year,month,day];
//    
////    year = [calendar component:NSCalendarUnitYear fromDate:TimeObject.STARTDATE];
////    month = [calendar component:NSCalendarUnitMonth fromDate:TimeObject.STARTDATE];
////    day = [calendar component:NSCalendarUnitDay fromDate:TimeObject.STARTDATE];
//
    DESCL.text = [[NSString alloc] initWithFormat:@"%@",TimeObject.DESC];
    NAMEL.text = [[NSString alloc] initWithFormat:@"%@",TimeObject.NAME];
    [IsCompleted setUserInteractionEnabled:NO];
    [IsPublished setUserInteractionEnabled:NO];
    [ONTL setUserInteractionEnabled:NO];
    
    [Edit setEnabled:YES];
    [Update setEnabled:NO];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField==bubbleDate) {
        UIToolbar*keyboardToolbar;
        if (keyboardToolbar == nil) {
            keyboardToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
            [keyboardToolbar setBarStyle:UIBarStyleDefault];
            
            UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
            
            UIBarButtonItem *accept = [[UIBarButtonItem alloc] initWithTitle:@"Ok" style:UIBarButtonItemStyleDone target:self action:@selector(ChangedDate)];
            
            [keyboardToolbar setItems:[[NSArray alloc] initWithObjects: extraSpace, accept, nil]];
        }
        textField.inputAccessoryView = keyboardToolbar;
        Datepicker = [[UIDatePicker alloc] init] ;
        CGRect fr=Datepicker.frame;
        fr.size.height=150;
        Datepicker.frame=fr;
        Datepicker.datePickerMode = UIDatePickerModeDate;
        //    [Datepicker addTarget:self action:@selector(ChangedDate) forControlEvents:UIControlEventValueChanged];
        textField.inputView=Datepicker;
        
        NSDate *NowDate=TimeObject.bubbleDate;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd MMMM yy"];
        //    NSString *toDay=[[dateFormatter stringFromDate:NowDate] uppercaseString];
        Datepicker.date=NowDate;
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
}

-(IBAction)ChangedDate
{
    
    [bubbleDate resignFirstResponder];
    NSDate *NowDate=Datepicker.date;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMM yy"];
    NSString *toDay=[[dateFormatter stringFromDate:NowDate] uppercaseString];
    NSRange boldedRange = NSMakeRange(3, toDay.length-6);
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:toDay];
    
    [attrString addAttribute:NSFontAttributeName
                       value:[UIFont systemFontOfSize:30.0]
                       range:boldedRange];
    
    
    [bubbleDate setAttributedText:attrString];
//    myBubble.bubbleDate=Datepicker.date;
    
}

#pragma mark - UITableViewDataSource, UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

        return   UITableViewAutomaticDimension;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView==bubblesTable) {
        return [linkedBubbles count]+[InlinkedBubbles count];
    }
        return [listUsers count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
        BubbleLinkedCell*cell=[tableView dequeueReusableCellWithIdentifier:@"mycell"];
        [cell initCell];
    if (tableView==bubblesTable) {
        Bubble*bubble;
        
        if (indexPath.row<[linkedBubbles count]) {
            bubble=[linkedBubbles objectAtIndex:indexPath.row];
           
        }else
            bubble=[InlinkedBubbles objectAtIndex:indexPath.row-[linkedBubbles count]];
        
        cell.bubble=bubble;
         cell.cellTitle.text=bubble.NAME;
        if ([toRemoveArray containsObject:bubble]||[linkedBubbles containsObject:bubble]) {
            cell.ischecked=YES;
            cell.checked.alpha=1;
        }else{
            cell.ischecked=NO;
            cell.checked.alpha=0;
        }

    }else
        cell.cellTitle.text=[[listUsers objectAtIndex:indexPath.row] objectForKey:@"NAME"];
        
        return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
     BubbleLinkedCell*cell=[tableView cellForRowAtIndexPath:indexPath];
    if (tableView==bubblesTable) {
        
        if (cell.ischecked) {
            if ([toAddArray containsObject:cell.bubble]) {
                [toAddArray removeObject:cell.bubble];
            }
            else{
                [toRemoveArray addObject:cell.bubble];
            }
            
        }
        else{
            if ([toRemoveArray containsObject:cell.bubble]) {
                [toRemoveArray removeObject:cell.bubble];
            }else{
                [toAddArray addObject:cell.bubble];
            }
        }
        cell.checked.alpha=cell.ischecked?0:1;
        cell.ischecked=!cell.ischecked;
        
    }else{
       
        if (!usersSelected) {
            usersSelected=[NSMutableArray array];
        }
        
        NSString*userId=[[listUsers objectAtIndex:indexPath.row] objectForKey:@"ID"];
        if ([usersSelected containsObject:userId]) {
            [usersSelected removeObject:userId];
            cell.checked.alpha=0;
        }else{
            [usersSelected addObject:userId];
            cell.checked.alpha=1;
        }
    }

}

#pragma mark UIAlertView delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    //Share it
    if (alertView.tag==0)
    {
        if (buttonIndex==0)
        {
            NSString *CORE_To_Share = [[NSString alloc] initWithFormat:@"Dear,\n\n\n In %@, My %@ is %@ in %@.\n\n",TimeObject.bubbleDate,TimeObject.KIND,TimeObject.DESC,TimeObject.NAME];
            NSArray *objectsToShare = @[CORE_To_Share];
            
            UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:@[objectsToShare,CORE_To_Share] applicationActivities:nil];
            
            // Present the controller
            [self presentViewController:activityController animated:YES completion:nil];
        }
    }else{
          [editDescView removeFromSuperview];
    }
}

- (void) dismissKeyboard
{
    [NAMEL resignFirstResponder];
    [DESCL resignFirstResponder];
//    [TEXTCOMMENT resignFirstResponder];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
//    if(textField==TEXTCOMMENT)
//        [textField setReturnKeyType:UIReturnKeySend];
}

#pragma mark -UITextView delegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    

    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    UIToolbar * keyboardToolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
    
    keyboardToolBar.barStyle = UIBarStyleDefault;
    [keyboardToolBar setItems: [NSArray arrayWithObjects:
                                [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(resignKeyboard)],
                                nil]];
    textView.inputAccessoryView = keyboardToolBar;
    
    return YES;
    
}

-(void)resignKeyboard{
    [descTextView resignFirstResponder];
}


- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (range.length == 0) {
        if ([text isEqualToString:@"\n"]) {
            textView.text = [NSString stringWithFormat:@"%@\n",textView.text];
            return NO;
        }
    }
    
    return YES;
}


#pragma mark keyboardnotification

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    
    if (descTextView) {
        NSDictionary* info = [aNotification userInfo];
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
        SCROLL.contentInset = contentInsets;
        SCROLL.scrollIndicatorInsets = contentInsets;
        
        CGRect fr=descTextView.frame;
        fr.size.height=fr.size.height/2;
        descTextView.frame=fr;
        
        [SCROLL scrollRectToVisible:descTextView.frame animated:YES];
    }
    
}

- (void)keyboardWillHide:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    SCROLL.contentInset = contentInsets;
    SCROLL.scrollIndicatorInsets = contentInsets;
}



#pragma mark Actions
-(IBAction)removeBubblesView:(id)sender{
    [bubblesView removeFromSuperview];
}

-(IBAction)Done:(id)sender{
    
    NSLog(@"toAddArray count %d",[toAddArray count]);
    
    for (Bubble*b in toAddArray) {
        b.PID=TimeObject.ID;
         [[Singleton sharedInstance] UpdateParentId:b.PID withID:b.ID];
    }
}
//-(IBAction)Action:(UIButton*)sender{
//    if (sender.tag==0) {
//       
//        NSArray *menuItems =
//        @[
//          [KxMenuItem menuItem:@"Share" image:nil target:self action:@selector(Share)],
//          [KxMenuItem menuItem:@"Edit" image:nil target:self action:@selector(edit)],
//          [KxMenuItem menuItem:@"Delete" image:nil target:self action:@selector(delete)],
//          [KxMenuItem menuItem:@"Linked" image:nil target:self action:@selector(getLinked)],
//          ];
//        CGRect fr=sender.frame;
//        fr.origin.y+=20;
//        [KxMenu showMenuInView:self.view fromRect:fr menuItems:menuItems];
//        
//    }
//    else  if (sender.tag==2) {
//        [self Edit:sender];
//        actionButton.tag=0;
//        [actionButton setTitle:@"Actions" forState:UIControlStateNormal];
//        NAMEL.userInteractionEnabled = YES;
//        DESCL.alpha=1;
//         DESCL.text=[[NSString alloc] initWithFormat:@"%@",descTextView.text];
//        
//        descTextView.alpha=0;
//        if([descTextView isFirstResponder]){
//            [descTextView resignFirstResponder];
//        }
//    }
//}
//
- (void)getLinked{
    linkedBubbles=[[Singleton sharedInstance] BubblesWithParentID:TimeObject.ID];
    InlinkedBubbles=[[Singleton sharedInstance] BubblesWithParentID:[NSNumber numberWithInt:0]];
    
    bubblesView.frame=self.view.bounds;
    [self.view addSubview:bubblesView];
 
}
-(void)Share{
    
    [SGActionView showGridMenuWithTitle:[NSString stringWithFormat:@"Share :%@",TimeObject.NAME ]
                             itemTitles:@[ @"Gradumate",@"Facebook", @"Twitter", @"Google+", @"Linkedin", @"Dropbox" ]
                                 images:@[
                                          [UIImage imageNamed:@"58.png"],
                                          [UIImage imageNamed:@"facebook"],
                                          [UIImage imageNamed:@"twitter"],
                                          [UIImage imageNamed:@"googleplus"],
                                          [UIImage imageNamed:@"linkedin"],
                                          [UIImage imageNamed:@"dropbox"]]
                         selectedHandle:^(NSInteger index){
                             if (index!=0) {
                                 [self shareBubble];
                                 NSLog(@"%d",index);
                             }
                             
                         }];
}

-(void)shareBubble{
    
    NSNumber *MyUserID=[[[NSUserDefaults standardUserDefaults] objectForKey:@"GRAD_USER"] objectForKey:@"ID"];

    [JSONHelper GetListUsersWithId:MyUserID delegate:self tag:0];
    
}

-(IBAction)ConfirmSharing:(id)sender{
    if ([usersSelected count]==0) {
        [[[UIAlertView alloc]initWithTitle:@"" message:@"You should at least select one user and add bubble title!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
    }else if(bubbleTitle.text.length==0){
        
         [[[UIAlertView alloc]initWithTitle:@"" message:@"You should add a title for the bubble!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        
    }else{
        
        NSString * combinedStuff = [usersSelected componentsJoinedByString:@","];
        
        NSLog(@"%@",combinedStuff);
        [JSONHelper ShareBubble:TimeObject title:bubbleTitle.text withUsers:combinedStuff delegate:self tag:1];
        
    }
    
}

-(IBAction)backFromUsersList:(id)sender{
    [usersV removeFromSuperview];
}

-(void)receiveData:(id)responseData tag:(int)tag{
    
    if (tag==0) {
        NSLog(@"%@",responseData);
        listUsers=responseData;
        usersV.frame=self.view.bounds;
        [self.view addSubview:usersV];
    }else {
        NSLog(@"%@",responseData);
        if ([[responseData objectForKey:@"ETAT"] isEqualToString:@"YES"]) {
            [usersV removeFromSuperview];
            [[[UIAlertView alloc] initWithTitle:nil message:@"Share succeed" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            
        }else
            [[[UIAlertView alloc] initWithTitle:nil message:@"Would you please repeat" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
}
-(void)delete{
    
    [[[UIAlertView alloc]initWithTitle:@"" message:@"Coming soon..." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
}
-(void)edit{
  
    NAMEL.userInteractionEnabled = YES;
    descTextView.userInteractionEnabled = YES;
    [descTextView setText:[[NSString alloc] initWithFormat:@"%@",DESCL.text]];
//     [DESCL removeFromSuperview];
    descTextView.alpha=1;
    DESCL.alpha=0;
    
    [IsCompleted setUserInteractionEnabled:YES];
    [IsPublished setUserInteractionEnabled:YES];
    [ONTL setUserInteractionEnabled:YES];
    actionButton.tag=2;
    [actionButton setTitle:@"Done" forState:UIControlStateNormal];
}
-(void)initDescriptionView{
    bubbleTitle.text=[[NSString alloc] initWithFormat:@"%@",TimeObject.NAME];
    [descTextView setText:[[NSString alloc] initWithFormat:@"%@",TimeObject.DESC]];
}

#pragma mark description view

-(IBAction)EditDescription:(id)sender{
    [self initDescriptionView];
    editDescView.frame=CGRectMake(20, 64, self.view.frame.size.width-40, self.view.frame.size.height-80);
    [self.view addSubview:editDescView];
}

-(IBAction)ConfirmEditDescription:(id)sender{
    if ([NAMEL.text isEqualToString:@""]||[descTextView.text isEqual:@""])
    {
        [[[UIAlertView alloc] initWithTitle:@"Would you please fill all requirements" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }else{
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"GRADUMATE.sqlite"];
        FMDatabase* db = [FMDatabase databaseWithPath:writableDBPath];
        if(![db open])
        {
            NSLog(@"Err %d: %@",[db lastErrorCode],[db lastErrorMessage]);
        }
        
        [db setShouldCacheStatements:YES];
        
        ReloadTimeline=1;
        
        NSString*BNAME=bubbleTitle.text;
        NSString*BDESC=descTextView.text;
        
        [db executeUpdate:@"UPDATE GRAD_TIMELINE SET NAME=? WHERE ID=?",BNAME,TimeObject.ID,nil];
        [db executeUpdate:@"UPDATE GRAD_TIMELINE SET DESC=? WHERE ID=?",BDESC,TimeObject.ID,nil];
        
        UIAlertView*alert=[[UIAlertView alloc]initWithTitle:@"" message:@"bubble description edited successefully." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alert.tag=1;
       [alert show];
        
//
        
    }
}
-(IBAction)removeDescView:(id)sender{
    [editDescView removeFromSuperview];
}


-(void) InitAchievementImage
{

    if ([TimeObject.KIND isEqualToString:@"CERTIFICATE"])
    {
      AchievementImg.image =[UIImage imageNamed:@"TLcertificate"];
        AchievementImg.tag=1;
    }
    
    else if ([TimeObject.KIND isEqualToString:@"ACTIVITY"])
    {
      AchievementImg.image =[UIImage imageNamed:@"TLactivity"];
        AchievementImg.tag=2;
    }
    
    else if ([TimeObject.KIND isEqualToString:@"COURSE"])
    {
         AchievementImg.image =[UIImage imageNamed:@"TLcourse"];
        AchievementImg.tag=3;
    }
    
    else if ([TimeObject.KIND isEqualToString:@"VIDEO"])
    {
        AchievementImg.image =[UIImage imageNamed:@"TLvideo"];
        AchievementImg.tag=4;
    }

    else if ([TimeObject.KIND isEqualToString:@"GOAL"])
    {
        AchievementImg.image =[UIImage imageNamed:@"TLgoal"];
        AchievementImg.tag=5;
    }

    else if ([TimeObject.KIND isEqualToString:@"OBJECTIVE"])
    {
        AchievementImg.image =[UIImage imageNamed:@"TLobjective"];
        AchievementImg.tag=6;
    }
    
    else if ([TimeObject.KIND isEqualToString:@"EVENT"])
    {
        AchievementImg.image =[UIImage imageNamed:@"TLevent"];
        AchievementImg.tag=7;
    }

    else if ([TimeObject.KIND isEqualToString:@"ANNOUNCEMENT"])
    {
       AchievementImg.image =[UIImage imageNamed:@"TLannouncement"];
        AchievementImg.tag=8;
    }
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSInteger year = [calendar component:NSCalendarUnitYear fromDate:TimeObject.bubbleDate];
    NSInteger day = [calendar component:NSCalendarUnitDay fromDate:TimeObject.bubbleDate];
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM"];
    DATEL.text=[NSString stringWithFormat:@"%ld %@ %ld",(long)day,[[formatter stringFromDate:TimeObject.bubbleDate] uppercaseString],(long)year];
    BubbleLab.text=TimeObject.NAME;
    UniversityWebsite.text=[NSString stringWithFormat:@"@%@",TimeObject.LINK];
    
}

-(IBAction)BACK:(id)sender
{
//    ReloadTimeline

        [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)GENERATETL:(id)sender
{
    
//    LifeLineViewController *SUBTL  = [self.storyboard instantiateViewControllerWithIdentifier:@"SUB"];
//    SUBTL.modalPresentationStyle = UIModalPresentationCurrentContext;
//    SUBTL.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
//        
//    [SUBTL setTLID:TimeObject];
//    
//    //[self.navigationController pushViewController:SUBTL animated:YES];
//    [super presentViewController:SUBTL animated:YES completion:nil];
    
}

-(IBAction)delete:(id)sender
{
    [[Singleton sharedInstance]deleteBubbleWithID:TimeObject.ID];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    return;
}

-(IBAction)CANCEL_Cong_View:(id)sender
{
    [CongView setHidden:YES];
    CONGLABEL.text =@"";
    [CongView2 setHidden:YES];
}

- (IBAction) Update:(id)sender
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"GRADUMATE.sqlite"];
    FMDatabase* db = [FMDatabase databaseWithPath:writableDBPath];
    if(![db open])
    {
        NSLog(@"Err %d: %@",[db lastErrorCode],[db lastErrorMessage]);
    }
    
    [db setShouldCacheStatements:YES];
    
    if ([NAMEL.text isEqualToString:@""]||[DESCL isEqual:@""])
    {
        [[[UIAlertView alloc] initWithTitle:@"Would you please fill all requirements" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
    else
    {
        
        NAMEL.text = [[NSString alloc] initWithFormat:@"%@",NAMEL.text];
   
        [db executeUpdate:@"UPDATE GRAD_TIMELINE SET NAME=? WHERE ID=?",[[NSString alloc] initWithFormat:@"%@",NAMEL.text],TimeObject.ID,nil];
        [db executeUpdate:@"UPDATE GRAD_TIMELINE SET DESC=? WHERE ID=?",[[NSString alloc] initWithFormat:@"%@",DESCL.text],TimeObject.ID,nil];
        [db executeUpdate:@"UPDATE GRAD_TIMELINE SET AchDate=? WHERE ID=?",DATE.date,TimeObject.ID,nil];
        
        if (IsCompleted.isOn)
        {
            [db executeUpdate:@"UPDATE GRAD_TIMELINE SET IsCompleted=1 WHERE ID=?",TimeObject.ID,nil];
            CONGLABEL.text = [[NSString alloc] initWithFormat:@"Congratulation, you have achieved %@",TimeObject.NAME];
            TimeObject.IsCompleted=1;
            [CongView setHidden:NO];
            [CongView2 setHidden:NO];
        }
        else
        {

            TimeObject.IsCompleted=0;
            [db executeUpdate:@"UPDATE GRAD_TIMELINE SET IsCompleted=0 WHERE ID=?",TimeObject.ID,nil];
        }
        
        if (IsPublished.isOn)
        {
            [db executeUpdate:@"UPDATE GRAD_TIMELINE SET IsPublished=1 WHERE ID=?",TimeObject.ID,nil];
        }
        else
        {
            [db executeUpdate:@"UPDATE GRAD_TIMELINE SET IsPublished=0 WHERE ID=?",TimeObject.ID,nil];
        }
        
        if (ONTL.isOn)
        {
            [db executeUpdate:@"UPDATE GRAD_TIMELINE SET ONTL=1 WHERE ID=?",TimeObject.ID,nil];
        }
        else
        {
            [db executeUpdate:@"UPDATE GRAD_TIMELINE SET ONTL=0 WHERE ID=?",TimeObject.ID,nil];
        }
        
        [db close];
 
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSInteger year = [calendar component:NSCalendarUnitYear fromDate:DATE.date];
        NSInteger day = [calendar component:NSCalendarUnitDay fromDate:DATE.date];
        
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM"];
        NSString *MMM=[[formatter stringFromDate:TimeObject.bubbleDate] uppercaseString];
        
        DATEL.text=[NSString stringWithFormat:@"%ld %@ %ld",(long)day,MMM,year];
    }
    
    [self dismissKeyboard];
    
    return;
}

-(IBAction)SHARE_Button:(id)sender
{
    NSString *CORE_To_Share = [[NSString alloc] initWithFormat:@"Dear,\n\n\n In %@, My %@ is %@ in %@.\n\n",TimeObject.bubbleDate,TimeObject.KIND,TimeObject.DESC,TimeObject.NAME];
    NSArray *objectsToShare = @[CORE_To_Share];
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:@[objectsToShare,CORE_To_Share] applicationActivities:nil];
    
    // Present the controller
    [self presentViewController:activityController animated:YES completion:nil];
}

-(IBAction)GetReference:(id)sender
{
    REF_NUMBER=1;
    
//    RefViewController *REF = [[RefViewController alloc] init];
//    REF.modalPresentationStyle = UIModalPresentationFullScreen;
//    REF.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//    REF = [self.storyboard instantiateViewControllerWithIdentifier:@"REF"];
//    [REF setTLO:TimeObject];
//    [self presentViewController:REF animated:YES completion:nil];
}

-(IBAction)share:(id)sender
{
    CONGLABEL.text = [[NSString alloc] initWithFormat:@"You have achieved %@",TimeObject.NAME];
    [CongView setHidden:NO];
    [CongView2 setHidden:NO];
}

-(IBAction)Edit:(id)sender
{

            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"GRADUMATE.sqlite"];
            FMDatabase* db = [FMDatabase databaseWithPath:writableDBPath];
            if(![db open])
            {
                NSLog(@"Err %d: %@",[db lastErrorCode],[db lastErrorMessage]);
            }
        
            [db setShouldCacheStatements:YES];
        
            ReloadTimeline=1;
    

            [db executeUpdate:@"UPDATE GRAD_TIMELINE SET AchDate=? WHERE ID=?",TimeObject.bubbleDate,TimeObject.ID,nil];
            
            if (IsCompleted.isOn)
            {
                [db executeUpdate:@"UPDATE GRAD_TIMELINE SET IsCompleted=1 WHERE ID=?",TimeObject.ID,nil];
                CONGLABEL.text = [[NSString alloc] initWithFormat:@"Congratulation, you have achieved %@",TimeObject.NAME];
//                [Share setEnabled:YES];
                [CongView setHidden:NO];
                [CongView2 setHidden:NO];
            }
            else
            {
//                [Share setEnabled:NO];
                [CongView setHidden:YES];
                [CongView2 setHidden:YES];
                [db executeUpdate:@"UPDATE GRAD_TIMELINE SET IsCompleted=0 WHERE ID=?",TimeObject.ID,nil];
            }
            
            if (IsPublished.isOn)
            {
                [db executeUpdate:@"UPDATE GRAD_TIMELINE SET IsPublished=1 WHERE ID=?",TimeObject.ID,nil];
            }
            else
            {
                [db executeUpdate:@"UPDATE GRAD_TIMELINE SET IsPublished=0 WHERE ID=?",TimeObject.ID,nil];
            }
            
            if (ONTL.isOn)
            {
                [db executeUpdate:@"UPDATE GRAD_TIMELINE SET ONTL=1 WHERE ID=?",TimeObject.ID,nil];
            }
            else
            {
                [db executeUpdate:@"UPDATE GRAD_TIMELINE SET ONTL=0 WHERE ID=?",TimeObject.ID,nil];
            }
            
            [db close];
            
//            NSCalendar *calendar = [NSCalendar currentCalendar];
//            NSInteger year = [calendar component:NSCalendarUnitYear fromDate:DATE.date];
//            NSInteger month = [calendar component:NSCalendarUnitMonth fromDate:DATE.date];
//            NSInteger day = [calendar component:NSCalendarUnitDay fromDate:DATE.date];
//            
//            NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
//            [formatter setDateFormat:@"MMM"];
//              NSString *MMM=[[formatter stringFromDate:TimeObject.bubbleDate] uppercaseString];
//            
//            DATEL.text=[NSString stringWithFormat:@"%ld %@ %ld",(long)day,MMM,year];
            
            [DATE setUserInteractionEnabled:NO];
            [IsCompleted setUserInteractionEnabled:NO];
            [IsPublished setUserInteractionEnabled:NO];
            [ONTL setUserInteractionEnabled:NO];
  
        [self dismissKeyboard];
  
}

-(IBAction)GetCertif:(id)sender
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"GRADUMATE.sqlite"];
    FMDatabase* db = [FMDatabase databaseWithPath:writableDBPath];
    if(![db open])
    {
        NSLog(@"Err %d: %@",[db lastErrorCode],[db lastErrorMessage]);
    }
    
    [db setShouldCacheStatements:YES];
    
    [db executeUpdate:@"UPDATE GRAD_TIMELINE SET NBCOMMENT=NBCOMMENT+1 WHERE ID=?",[NSNumber numberWithInteger:TimeObject.ID],nil];
    
    int USER_ID=0;
    FMResultSet *R1 = [db executeQuery:@"SELECT* FROM GRAD_USER"];
    
    while ([R1 next])
    {
        USER_ID = [R1 intForColumn:@"ID"];
    }
    
    NSString *url = [[NSString alloc] initWithFormat:@"%@GetCertif?IDI=%ld&IDU=%i",@BASE_URL,(long)TimeObject.ID,USER_ID];
    
    NSDictionary *responseData = [JSONHelper parseJsonResponse:url];
    if (responseData)
    {
        NSLog(@"OK");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"SOON, You'll get a certificate from GRADUMATE" delegate:self cancelButtonTitle:@"Thanks for your trust" otherButtonTitles:nil, nil];
        [alert setTag:2];
        [alert show];
        [CongView setHidden:YES];
        [CongView2 setHidden:YES];
    }

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

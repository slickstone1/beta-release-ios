//
//  TLTableViewCell.m
//  GRADUMATE
//
//  Created by Ahmad Jarray on 19/11/2015.
//  Copyright © 2015 Ahmad Jarray. All rights reserved.
//

#import "TLTableViewCell.h"
#import "COMMENT.h"

#import <MDCShineEffect/UIView+MDCShineEffect.h>

@implementation TLTableViewCell

- (void)awakeFromNib {
    
    origninalcolor=timeLineView.backgroundColor;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(void)configwiThTimeLine:(Bubble*)timeLine future:(BOOL)future{
    _bubble=timeLine;
    if ([timeLine.KIND isEqualToString:@"CERTIFICATE"])
    {
        imageview .image=[UIImage imageNamed:@"TLcertificate"];
    }
    else if ([timeLine.KIND isEqualToString:@"ACTIVITY"])
    {
        imageview .image=[UIImage imageNamed:@"TLactivity"];
    }
    else if ([timeLine.KIND isEqualToString:@"COURSE"])
    {
        imageview .image=[UIImage imageNamed:@"TLcourse"];
    }
    else if ([timeLine.KIND isEqualToString:@"VIDEO"])
    {
        imageview .image=[UIImage imageNamed:@"TLvideo"];
    }
    
    //LEFT_SIDE
    else if ([timeLine.KIND isEqualToString:@"GOAL"])
    {
        imageview .image=[UIImage imageNamed:@"TLgoal"];
    }
    else if ([timeLine.KIND isEqualToString:@"OBJECTIVE"])
    {
        imageview .image=[UIImage imageNamed:@"TLobjective"];
    }
    else if ([timeLine.KIND isEqualToString:@"EVENT"])
    {
        imageview .image=[UIImage imageNamed:@"TLevent"];
    }
    else if ([timeLine.KIND isEqualToString:@"ANNOUNCEMENT"])
    {
        imageview .image=[UIImage imageNamed:@"TLannouncement"];
    }

    NSInteger year = [[NSCalendar currentCalendar] component:NSCalendarUnitYear fromDate:timeLine.bubbleDate];
    NSInteger month = [[NSCalendar currentCalendar] component:NSCalendarUnitMonth fromDate:timeLine.bubbleDate];
    NSInteger day = [[NSCalendar currentCalendar] component:kCFCalendarUnitDay fromDate:timeLine.bubbleDate];
    
    NSString *MMM = [self ReturnMounth:month];
    lABELDate.text = [NSString stringWithFormat: @"%d %@ %li",day,MMM,(long)year];
    lableName.text= timeLine.NAME;
    //    NSArray* comments=[self GetCommentsForBubble:timeLine foredit:0];
    //    if (comments) {
    //        COMMENT *comm= [comments lastObject];
    //        labelCom.text= comm.NAME;
    //    }else
    labelCom.text= timeLine.DESC;
    
    if (timeLine.IsCompleted ==1)
    {
        [self Shake: imageview ];
    }
    
    if (future) {
        timeLineView.backgroundColor=origninalcolor;
        
    }else{
        timeLineView.backgroundColor=[UIColor lightGrayColor];
    }
}

- (NSString *)ReturnMounth: (NSInteger)MOUNTH_F
{
    NSString *MOUNTH;
    switch (MOUNTH_F)
    {
        case 1:
            MOUNTH = @"JAN";
            break;
        case 2:
            MOUNTH = @"FEB";
            break;
        case 3:
            MOUNTH = @"MAR";
            break;
        case 4:
            MOUNTH = @"APR";
            break;
        case 5:
            MOUNTH = @"MAY";
            break;
        case 6:
            MOUNTH = @"JUN";
            break;
        case 7:
            MOUNTH = @"JUL";
            break;
        case 8:
            MOUNTH = @"AUG";
            break;
        case 9:
            MOUNTH = @"SEPT";
            break;
        case 10:
            MOUNTH = @"OCT";
            break;
        case 11:
            MOUNTH = @"NOV";
            break;
        case 12:
            MOUNTH = @"DEC";
            break;
            
        default:
            break;
    }
    
    return MOUNTH;
}

-(NSArray*)GetCommentsForBubble:(Bubble*) bubble foredit:(NSInteger)foredit
{
    NSArray *coms=[[Singleton sharedInstance]commentsForBubbleID:bubble.ID Locally:NO];
    if ([coms count]) {
        return coms;
    }
    return nil;
}

-(void)Shake:(UIImageView *)Image
{
    [Image shine];
    CABasicAnimation* shake = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    shake.fromValue = [NSNumber numberWithFloat:-0.1];
    shake.toValue = [NSNumber numberWithFloat:+0.1];
    shake.duration = 0.1;
    shake.autoreverses = YES;
    shake.repeatCount = 9;
    [Image.layer addAnimation:shake forKey:@"imageView"];
    
    //  dispatch_after(DISPATCH_TIME_NOW,  dispatch_get_main_queue(), ^(void){
    //      [Image setAlpha:0.8];
    //  });
    return;
}


@end

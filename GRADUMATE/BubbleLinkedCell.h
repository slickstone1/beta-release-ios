//
//  BubbleLinkedCell.h
//  GRADUMATE
//
//  Created by hfetoui on 24/12/2015.
//  Copyright © 2015 Ahmad Jarray. All rights reserved.
//

#import <UIKit/UIKit.h>


@class Bubble;
@interface BubbleLinkedCell : UITableViewCell


//-(void)ConfigCellWithBubble:(Bubble*)bubble;
//-(void)ConfigCellWithUser:(NSDictionary*)userDic;
-(void)initCell;


@property(nonatomic,retain)IBOutlet UIView*cellView;
@property(nonatomic,retain)IBOutlet UILabel*cellTitle;
@property(nonatomic,retain)IBOutlet UILabel*checked;
@property(nonatomic,assign) BOOL ischecked;
@property(nonatomic,assign) Bubble*bubble;


@end

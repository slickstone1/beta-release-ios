//
//  DBInitViewController.h
//  GRADUMATE
//
//  Created by Malek Mansour on 2/5/15.
//  Copyright (c) 2015 SlickStone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FMDatabase.h"

@interface DBInitViewController : UIViewController

-(void)START;
-(void)Insert;
-(void)forget;

@end

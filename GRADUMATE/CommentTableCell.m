//
//  SimpleTableCell.m
//  SimpleTable
//
//  Created by Simon Ng on 28/4/12.
//  Copyright (c) 2012 Appcoda. All rights reserved.
//

#import "CommentTableCell.h"
#import "COMMENT.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation CommentTableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code

    }
    return self;
}

-(void)awakeFromNib{
    messageContnerView.layer.cornerRadius=10;
    messageContnerView.layer.masksToBounds=YES;
    UserImg.layer.cornerRadius=UserImg.frame.size.width/2;
    UserImg.layer.masksToBounds=YES;

}

-(void)setComment:(COMMENT *)acomment{
    commentLabel.text=acomment.TEXT;
    
    
    [UserImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",acomment.userImage]] placeholderImage:[UIImage imageNamed:@"58"]];
    Username.text=acomment.userName;
//    acomment.userName
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  User.h
//  GRADUMATE
//
//  Created by Ahmad Jarray on 10/12/2015.
//  Copyright © 2015 Ahmad Jarray. All rights reserved.
//

@interface User : NSObject
@property(nonatomic,retain)NSString*name;
@property(nonatomic,retain)NSString*image;
@property(nonatomic,retain)NSNumber*ID;

+(instancetype)userFromDic:(NSDictionary*)dic;

@end

//
//  User.m
//  GRADUMATE
//
//  Created by Ahmad Jarray on 10/12/2015.
//  Copyright © 2015 Ahmad Jarray. All rights reserved.
//

#import "User.h"

@implementation User
+(instancetype)userFromDic:(NSDictionary*)dic{

    User*user=[[User alloc] init];
    user.name=[dic objectForKey:@"NAME"];
    user.image=[dic objectForKey:@"IMAGE"];
    user.ID=[dic objectForKey:@"ID"];
    
    return user;
}

@end

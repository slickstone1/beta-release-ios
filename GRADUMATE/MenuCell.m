//
//  MenuCell.m
//  GRADUMATE
//
//  Created by Ahmad Jarray on 11/12/2015.
//  Copyright © 2015 Ahmad Jarray. All rights reserved.
//

#import "MenuCell.h"

@implementation MenuCell

-(void)awakeFromNib{
    imageview.layer.masksToBounds=YES;
    labelName.textColor=PRINCIPAL_BORDER_COLOR;
    labelNotification.layer.borderWidth=2;
    labelNotification.layer.borderColor=[PRINCIPAL_BORDER_COLOR CGColor];
}

-(void)configWithImage:(UIImage*)aimage title:(NSString*)title notification:(NSString*)n{
    imageview.image=aimage;
     labelName.text=title;
    
    if ([n integerValue]) {
        labelNotification.alpha=1;
        labelNotification.text=n;
        
    }else
        labelNotification.alpha=0;

    
   
}


- (void)drawRect:(CGRect)rect
{
    imageview.layer.cornerRadius=imageview.frame.size.height/2;
    labelNotification.layer.cornerRadius=labelNotification.frame.size.height/2;
    
    // Drawing code
}



@end

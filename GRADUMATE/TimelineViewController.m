//
//  LifeLineViewController.m
//  GRADUMATE
//
//  Created by Jarray on 10/11/2015.
//  Copyright © 2015 SlickStone. All rights reserved.
//


#import "TimelineViewController.h"
#import "FMDatabase.h"
#import "JSONHelper.h"
#import "ContextMenuCell.h"
#import "DBInitViewController.h"
#import "TLTableViewController.h"
#import "SeeMoreViewController.h"
#import "AddBubbleViewController.h"
#import <SGActionView/SGActionView.h>
#import "Singleton.h"
#import "BubbleLinkedCell.h"

#import "User.h"

#import "TLTableViewCell.h"

 @interface TimelineViewController ()
{
    TLTableViewController*  TL;
    BOOL todayfound;
    int  todayrow;
    void (^runblockforsave)(NSDate*) ;
    BOOL otherUser;
    NSMutableDictionary* data;
    Bubble*myBubbleToShare;
}

@end
@implementation TimelineViewController

- (void) viewDidLoad
{
    
    [super viewDidLoad];
    
    
    if (otherUser) {
        menuWidth.constant=0 ;
        backBtn.alpha=1;
        addBTN.alpha=0;
        EditBtn.alpha=0;
    }
    data=[NSMutableDictionary dictionary];
    
    cornerView.layer.cornerRadius=cornerView.frame.size.width/2;
    if (otherUser) {
         [JSONHelper getTimeLineforUserId:_user.ID delegate:self tag:0];
    }else
        [JSONHelper getMyTimeLineDelegate:self tag:1];
      
    self.tabBarController.delegate=self;
    UITabBar *tabBar = self.tabBarController.tabBar;
    UITabBarItem *tabBarItem1 = [tabBar.items objectAtIndex:0];
    tabBarItem1.tag=1;
    tabBarItem1.accessibilityHint=@"1";
    contextMenuTableView.animationDuration = 0.15;
    contextMenuTableView.menuItemsSide = Right;
    contextMenuTableView.menuItemsAppearanceDirection = FromTopToBottom;
    tLTable.estimatedRowHeight = 250.0;
    self.view.backgroundColor=PRINCIPAL_TINT_COLOR;
    NSDictionary *user=[[NSUserDefaults standardUserDefaults] objectForKey:@"GRAD_USER"];
    if (user) {
        MyUserID=[[user objectForKey:@"ID"] intValue];
    }
    
    myBubbleToShare=[[Bubble alloc]init];
    [EditContainer setHidden:YES];
    LINE.image  =[LINE.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [LINE setTintColor:[UIColor colorWithRed:0x5A/255.f green:0x8C/255.f blue:0xA8/255.f alpha:1]];
    
    [SettingsBtn1 setClipsToBounds:YES];
    SettingsBtn1.layer.cornerRadius = SettingsBtn1.frame.size.width/2;
    [SearchBAR setClipsToBounds:YES];
    SearchBAR.layer.cornerRadius = 5;
    
    [CreateBubbleBtn.layer setMasksToBounds:YES];
    [CreateBubbleBtn.layer setCornerRadius:CreateBubbleBtn.frame.size.width/2];
    CreateBubbleBtn.backgroundColor=PRINCIPAL_TINT_COLOR;
    
    menuTitles = @[@"",@"GOAL", @"OBJECTIVE",@"CERTIFICATE", @"COURSE",@"EVENT", @"ACTIVITY"];
    
    menuIcons = @[[UIImage imageNamed:@"add-exit"],
                       [UIImage imageNamed:@"add-goal"],
                       [UIImage imageNamed:@"add-objective"],
                       [UIImage imageNamed:@"add-certification"],
                       [UIImage imageNamed:@"add-courses"],
                       [UIImage imageNamed:@"add-event"],
                       [UIImage imageNamed:@"add-activity"]];
    
    canMove=YES;
    [tLTable endUpdates];
}

-(void)receiveData:(id)responseData tag:(int)tag{
    
    if (tag==2) {
         NSLog(@"%@",responseData);
        listUsers=responseData;
        usersView.frame=self.view.bounds;
        [self.view addSubview:usersView];
    }else if (tag==3){
         NSLog(@"%@",responseData);
        if ([[responseData objectForKey:@"ETAT"] isEqualToString:@"YES"]) {
            [usersView removeFromSuperview];
            [[[UIAlertView alloc] initWithTitle:nil message:@"Share succeed" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            
        }else
          [[[UIAlertView alloc] initWithTitle:nil message:@"Would you please repeat" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
    else{
        [[Singleton sharedInstance]updateBubblesFromArray:responseData forUserID:_user?_user.ID:nil];
        
        [self updateTimeline];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    {
        [self Connect];
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    }
}

-(void)Connect
{
    //connect & update device token
    NSString *token=[[NSUserDefaults standardUserDefaults] valueForKey:@"deviceToken"];
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        NSString *url = [[NSString alloc] initWithFormat:@"%@Connect/%ld/%@",@BASE_URL,MyUserID,token];
        [JSONHelper parseJsonResponse:url withDelegate:self];
        
    });
}

-(void)receiveData:(id)responseData{
    if ([responseData isKindOfClass:[NSDictionary class]])
    {
        if ([[responseData valueForKey:@"ETAT"] isEqualToString:@"TENDAYS"])
        {
            NSString *welcomeMsg = [[NSString alloc] initWithFormat:@"WELCOME BACK %@",[responseData valueForKey:@"NAME"]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:welcomeMsg message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert setTag:4];
            [alert show];
        }
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [self updateTimeline];
}
- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    SearchBAR.text = @"";
    [SearchBAR resignFirstResponder];
    [self updateTimeline];
    return NO;
}

-(void)updateTimeline
{
    if (otherUser) {
      TimeLinelist= [[Singleton sharedInstance] timeLineForUser:_user.ID];
        
    }else
       TimeLinelist= [[Singleton sharedInstance] myTimeLine];
        BOOL needsort=NO;
        NSDictionary *userid=[[NSUserDefaults standardUserDefaults] objectForKey:@"GRAD_USER"];
        NSString*key=[NSString stringWithFormat:@"non_synchronised_bubbles_%@",userid];
        for (NSData *bubledata in [[NSUserDefaults standardUserDefaults] objectForKey:key]) {
            needsort=YES;
            Bubble*b = (Bubble *)[NSKeyedUnarchiver unarchiveObjectWithData:bubledata];
            [TimeLinelist addObject:b];
        }
    if (needsort) {
        //sort
        
        NSSortDescriptor *sortDescriptor=[[NSSortDescriptor alloc] initWithKey:@"bubbleDate"
                                                     ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        TimeLinelist = [[TimeLinelist sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
        
    }

      todayfound=NO;
    [tLTable reloadData];
    if(!todayfound){
    
        for (int i=0; i<[TimeLinelist count]; i++ ) {
            Bubble *bubble=[TimeLinelist objectAtIndex:i];
            if (!todayfound && [[NSDate date] compare:bubble.bubbleDate] == NSOrderedAscending) {
                todayfound=YES;
                todayrow=i;
               
            }
        }
    }
    [tLTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:todayrow inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
    return;
    
}
-(void)setUser:(User *)user{
    
    _user=user;
    otherUser=YES;
}

#pragma mark- UITabBarController delagte

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    UITabBar *tabBar = tabBarController.tabBar;
    UITabBarItem *tabBarItem1 = [tabBar.items objectAtIndex:0];
    if(tabBarController.selectedIndex==0)
    {
        if(tabBarItem1.accessibilityHint.intValue==1)
        {
//            if(tabBarItem1.tag==0)
//            {
//                TL.navigationController.viewControllers=@[self];
//                tabBarItem1.tag=1;
//            }
//            else
//            {
//                if (!TL) {
//                    TL = [self.storyboard instantiateViewControllerWithIdentifier:@"TLLIST"];
//                    TL.delegate=self;
//                }
//                self.navigationController.viewControllers=@[TL];
//                tabBarItem1.tag=0;
//            }
        }
        tabBarItem1.accessibilityHint=@"1";
    }
    else
        tabBarItem1.accessibilityHint=@"0";
}

#pragma mark - IBAction


-(IBAction)back:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(IBAction)backFromUsersList:(id)sender{
    [usersView removeFromSuperview];
}

- (IBAction)presentMenuButtonTapped:(UIBarButtonItem *)sender
{
    UIViewController *rootController = [[UIApplication sharedApplication] keyWindow].rootViewController;
    [contextMenuTableView showInView:rootController.view withEdgeInsets:UIEdgeInsetsZero animated:YES];
}

-(void)swipeInCell:(TLTableViewCell*)sender{
    
    [SGActionView showGridMenuWithTitle:[NSString stringWithFormat:@"Share :%@",sender.bubble.NAME ]
                             itemTitles:@[ @"Gradumate",@"Facebook", @"Twitter", @"Google+", @"Linkedin", @"Dropbox" ]
                                 images:@[
                                           [UIImage imageNamed:@"58.png"],
                                           [UIImage imageNamed:@"facebook"],
                                           [UIImage imageNamed:@"twitter"],
                                           [UIImage imageNamed:@"googleplus"],
                                           [UIImage imageNamed:@"linkedin"],
                                           [UIImage imageNamed:@"dropbox"]]
                         selectedHandle:^(NSInteger index){
                             if (index!=0) {
                                 [self shareBubble:sender ForShareIndex:index];
                                 myBubbleToShare=sender.bubble;
                                 NSLog(@"%d",index);
                             }
                         }];
}

-(void)shareBubble:(Bubble*)bubble ForShareIndex:(NSInteger)index{
    [JSONHelper GetListUsersWithId:_user.ID delegate:self tag:2];
 
}
-(IBAction)shareBubble:(id)sender{
    if ([usersSelected count]==0) {
        [[[UIAlertView alloc]initWithTitle:@"" message:@"You should at least select one user and add bubble title!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
    }else if(bubbleTitle.text.length==0){
        
        [[[UIAlertView alloc]initWithTitle:@"" message:@"You should add a title for the bubble!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        
    }else{
        
        NSString * combinedStuff = [usersSelected componentsJoinedByString:@","];
        
        NSLog(@"%@",combinedStuff);
        [JSONHelper ShareBubble:myBubbleToShare title:bubbleTitle.text withUsers:combinedStuff delegate:self tag:3];
        
    }
}

-(IBAction)didSwipe:(UIGestureRecognizer *)gestureRecognizer {
    
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        CGPoint swipeLocation = [gestureRecognizer locationInView:tLTable];
        NSIndexPath *swipedIndexPath = [tLTable indexPathForRowAtPoint:swipeLocation];
        TLTableViewCell* swipedCell = [tLTable cellForRowAtIndexPath:swipedIndexPath];
        NSLog(@"%@",swipedCell.bubble.NAME);
        [self swipeInCell:swipedCell];
        //        swipedCell.bubble.NAME;
        // ...
    }
}

- (IBAction) ShowSideMenu:(id)sender
{
    [[Singleton sharedInstance].drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];

}


//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//{
//   
//        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
//        [self updateTextLabelsWithText: newString];
//    
//    return YES;
//}
//-(void)updateTextLabelsWithText:(NSString *)string
//{
//
//    TimeLinelist= [[Singleton sharedInstance] bubbleWithSearshKey:string];
//    todayfound=nil;
//    [tLTable reloadData];
//    
//    if ([TimeLinelist count]==0)
//    {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Not item Found" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert setTag:3];
//        [alert show];
//    }
//
//}
-(IBAction)SEARCH:(id)sender
{
    TimeLinelist= [[Singleton sharedInstance] bubbleWithSearshKey:SearchBAR.text];
    todayfound=nil;
    [tLTable reloadData];
    
    if ([TimeLinelist count]==0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Not item Found" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert setTag:3];
        [alert show];
    }
}

-(IBAction)ShowSearch:(UIButton*)sender
{
    if(sender.tag==0)   //show
    {
        [UIView animateWithDuration:1.3f animations:^{
            seqrshHeight.constant=30;
        }];
        [SearchBtn setBackgroundImage:[UIImage imageNamed:@"search_selected.png"] forState:UIControlStateNormal];
        [SearchBAR becomeFirstResponder];
    }
    else
    {
        [UIView animateWithDuration:1.3f animations:^{
            seqrshHeight.constant=0;
        }];
        [SearchBtn setBackgroundImage:[UIImage imageNamed:@"search.png"] forState:UIControlStateNormal];
        [SearchBAR resignFirstResponder];
    }
    SearchBtn.tag=sender.tag?0:1;
    SearchBtn2.tag=sender.tag?0:1;
}

//-(IBAction)EditTimeLine:(id)sender
//{
//    if (EditBtn.selected) {
//        [tLTable endUpdates];
//    }
//    
//    [EditBtn setSelected:!EditBtn.selected];
//    canMove=EditBtn.selected;
//}

-(IBAction)SaveAfterEditMode:(id)sender
{
    runblockforsave(DATEEdit_Mode.date);
    [EditContainer setHidden:YES];
   
    [EditBtn setSelected:0];
    [self updateTimeline];
}

-(IBAction)CancelEditMode:(id)sender
{
    [EditContainer setHidden:YES];
    EditBtn.tag=0;
    [self updateTimeline];
}

#pragma mark - Local methods

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}
#pragma mark - UITableViewDataSource, UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
       if (tableView==tLTable) {
           if (!otherUser) {
                [self performSegueWithIdentifier:@"Detail" sender:indexPath];
           }
       
       }else if (tableView==usersTable){
           BubbleLinkedCell*cell=[tableView cellForRowAtIndexPath:indexPath];

           if (!usersSelected) {
               usersSelected=[NSMutableArray array];
           }
           
           NSString*userId=[[listUsers objectAtIndex:indexPath.row] objectForKey:@"ID"];
           if ([usersSelected containsObject:userId]) {
               [usersSelected removeObject:userId];
               cell.checked.alpha=0;
           }else{
               [usersSelected addObject:userId];
               cell.checked.alpha=1;
           }
           
       }else{
        [(YALContextMenuTableView*)tableView dismisWithIndexPath:indexPath];
        if (indexPath.row){

            [self performSegueWithIdentifier:@"addBubble" sender:indexPath];
        }
    }
}

-(BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return canMove;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ((tableView==tLTable)||(tableView==usersTable)) {
        return   UITableViewAutomaticDimension;
    }

    return 64;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView==tLTable)
        return   [TimeLinelist count]+1;
    else if (tableView==usersTable)
        return [listUsers count];
        
     return menuTitles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView==tLTable) {
        int bindex=indexPath.row;
        if (todayfound && indexPath.row > todayrow) {
            bindex=indexPath.row-1;
        }
                TLTableViewCell *cell;
        if (!TimeLinelist ||[TimeLinelist count]==0 || bindex>=[TimeLinelist count]) {
            cell=[tableView dequeueReusableCellWithIdentifier:@"elmToday"];
            return cell;
        }
         Bubble*bubble=[TimeLinelist objectAtIndex:bindex];
        
        if (!todayfound && [[NSDate date] compare:bubble.bubbleDate] == NSOrderedAscending) {
            todayfound=YES;
            todayrow=indexPath.row;
        }

        if (todayfound &&todayrow==indexPath.row) {
             cell=[tableView dequeueReusableCellWithIdentifier:@"elmToday"];
            return cell;
        }
        
        cell=[tableView dequeueReusableCellWithIdentifier:@"elm"];
        
        if (todayfound) {
            [cell configwiThTimeLine:bubble future:YES];
        }else
            [cell configwiThTimeLine:bubble future:NO];
       
        return cell;
    }else if (tableView==usersTable){
        
        BubbleLinkedCell*cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
        [cell initCell];
        cell.cellTitle.text=[[listUsers objectAtIndex:indexPath.row] objectForKey:@"NAME"];
        
        return cell;
        
    }else{
        ContextMenuCell*cell=[tableView dequeueReusableCellWithIdentifier:@"rotationCell"];
        cell.menuTitleLabel.text = [menuTitles objectAtIndex:indexPath.row];
        cell.menuImageView.image = [menuIcons objectAtIndex:indexPath.row];
        return cell;
    }
    return nil;
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleNone;

}
#pragma mark DragAndDropTableViewDelegate

-(void)tableView:(UITableView *)tableView willBeginDraggingCellAtIndexPath:(NSIndexPath *)indexPath placeholderImageView:(UIImageView *)placeHolderImageView
{
    placeHolderImageView.layer.shadowOpacity = .5;
    placeHolderImageView.layer.shadowRadius = 1;
}

-(void)tableView:(UITableView *)tableView didEndDraggingCellAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)toIndexPath placeHolderView:(UIImageView *)placeholderImageView
{
    if (canMove) {
        [DATEEdit_Mode setMaximumDate:nil];
        [DATEEdit_Mode setMinimumDate:nil];
        int index1=(int)toIndexPath.row;
        if (sourceIndexPath.row>toIndexPath.row) index1=(int)toIndexPath.row-1;
        if (index1>=0 && toIndexPath.row>0) {
            Bubble *fb1=[TimeLinelist objectAtIndex:index1];
            [DATEEdit_Mode setMinimumDate:fb1.bubbleDate];
        }
        if ((index1+1)<[TimeLinelist count] && toIndexPath.row<[TimeLinelist count]-1) {
            Bubble *fb2=[TimeLinelist objectAtIndex:index1+1];
            [DATEEdit_Mode setMaximumDate:fb2.bubbleDate];
        }
        [EditContainer setHidden:NO];
        runblockforsave= ^(NSDate *date){
            Bubble *o = [TimeLinelist  objectAtIndex:sourceIndexPath.row];
            o.bubbleDate=date;
            [TimeLinelist removeObjectAtIndex:sourceIndexPath.row];
            [TimeLinelist insertObject:o atIndex:toIndexPath.row];
            [[Singleton sharedInstance] updateDateForBubbleWithID:o.ID withDate:DATEEdit_Mode.date];
            NSLog(@"date change");
        };
    }
}
#pragma mark - prepareForSegue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     if ([segue.identifier isEqualToString:@"comment"]) {
        [(SeeMoreViewController*)segue.destinationViewController setTimeObject:[TimeLinelist objectAtIndex: ((NSIndexPath* )sender).row]];
    }else  if ([segue.identifier isEqualToString:@"Detail"]) {
        SeeMoreViewController*SeeMoreView=segue.destinationViewController;
        int bindex=((NSIndexPath*)sender).row;
        if (todayfound && ((NSIndexPath*)sender).row > todayrow) {
            bindex=((NSIndexPath*)sender).row-1;
        }
        [SeeMoreView setTimeObject:[TimeLinelist objectAtIndex:bindex]];
        SeeMoreView.AllBubbles=TimeLinelist;
        
    }
        else if ([segue.identifier isEqualToString:@"addBubble"]) {
        AddBubbleViewController*AddBubbleView= segue.destinationViewController;
        AddBubbleView.BubbleType=((NSIndexPath*)sender).row;
    }
    else{
    }
}

@end
